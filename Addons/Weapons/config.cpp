class CfgPatches {
	class PMC_Weapons {
		name = "Private Military Vehicles";
		author = "Optio";
		requiredAddons[] = {
			"PMC_Common"
		};
        units[] = {};
        weapons[] = {
			"PMC_rifle_01",
			"PMC_rifle_01_gl",
			"PMC_rifle_02",
			"PMC_rifle_03"
		};
		requiredVersion = 0.1;
	};
};


class CfgWeapons {
	class arifle_SPAR_01_blk_F;
	class arifle_SPAR_01_GL_blk_F;
	class arifle_SPAR_02_blk_F;
	class arifle_SPAR_03_blk_F;

	class PMC_rifle_01: arifle_SPAR_01_blk_F {
		class LinkedItems {
			class LinkedItemsOptic {
				slot = "CowsSlot";
				item = "optic_Holosight_blk_F";
			};
		};
		author = "Bohemia Interactive";
		_generalMacro = "PMC_rifle_01";
		baseWeapon = "arifle_SPAR_01_blk_F";
		scope = 1;
	};
	
	class PMC_rifle_01_gl: arifle_SPAR_01_GL_blk_F {
		class LinkedItems {
			class LinkedItemsOptic {
				slot = "CowsSlot";
				item = "optic_Holosight_blk_F";
			};
		};
		author = "Bohemia Interactive";
		_generalMacro = "PMC_rifle_01_gl";
		baseWeapon = "arifle_SPAR_01_GL_blk_F";
		scope = 1;
	};
	
	class PMC_rifle_02: arifle_SPAR_02_blk_F {
		class LinkedItems {
			class LinkedItemsOptic {
				slot = "CowsSlot";
				item = "optic_Holosight_blk_F";
			};
			class LinkedItemsUnder {
				slot = "UnderBarrelSlot";
				item = "bipod_02_F_blk";
			};
		};
		author = "Bohemia Interactive";
		_generalMacro = "PMC_rifle_02";
		baseWeapon = "arifle_SPAR_02_blk_F";
		scope = 1;
	};
	
	class PMC_rifle_03: arifle_SPAR_03_blk_F {
		class LinkedItems {
			class LinkedItemsOptic {
				slot = "CowsSlot";
				item = "optic_DMS";
			};
			class LinkedItemsUnder {
				slot = "UnderBarrelSlot";
				item = "bipod_02_F_blk";
			};
		};
		author = "Bohemia Interactive";
		_generalMacro = "PMC_rifle_03";
		baseWeapon = "arifle_SPAR_03_blk_F";
		scope = 1;
	};
};
