class CfgVehicles {
	// Ground items
	#define MACRO_ITEM_COMMON \
		dlc = "PMC"; \
		scope = 2; \
		scopeCurator = 2; \
		vehicleClass = "ItemsHeadgear";
	
	#define MACRO_ADDITEM(ITEM,COUNT) \
	class _xx_##ITEM { \
		name = #ITEM; \
		count = COUNT; \
	};
	
	#define QUOTE(var1) #var1
	#define CLASS(var1) PMC_##var1
	#define QCLASS(var1) QUOTE(CLASS(var1))
	#define CSTRING(var1) QUOTE($STR_PMC_Headgear_##var1)

	
	class Headgear_Base_F;
	class PMC_Headgear_MilCap_blk: Headgear_Base_F {
		scope = 2;
		scopeCurator = 2;
		displayName = CSTRING(MilCap_Black);
		author = "Bohemia Interactive, Optio";
		editorCategory = "EdCat_Equipment";
		editorSubcategory = "EdSubcat_Hats";
		vehicleClass = "ItemsHeadgear";
		model = "\A3\Weapons_F\DummyCap.p3d";
		class TransportItems {
			MACRO_ADDITEM(PMC_MilCap_blk,1)
		};
	};
		
	class CLASS(Item_Cap_Headphones_Dark): Headgear_Base_F {
		MACRO_ITEM_COMMON
		author = "Spartan Tactical Group, Jonpas";
		displayName = CSTRING(Cap_Headphones_Dark);
		model = "\A3\Weapons_F\DummyCap.p3d";
		class TransportItems {
			MACRO_ADDITEM(PMC_Cap_Headphones_Dark,1)
		};
	};
	
	class CLASS(Item_Cap_Dark): Headgear_Base_F {
		MACRO_ITEM_COMMON
		author = "Spartan Tactical Group, Jonpas";
		displayName = CSTRING(Cap_Dark);
		model = "\A3\Weapons_F\DummyCap.p3d";
		class TransportItems {
			MACRO_ADDITEM(PMC_Cap_Dark,1)
		};
	};
	
	class CLASS(Item_Cap_Earpiece_Dark): Headgear_Base_F {
		MACRO_ITEM_COMMON
		author = "Spartan Tactical Group, Jonpas";
		displayName = CSTRING(Cap_Earpiece_Dark);
		model = "\A3\Weapons_F\DummyCap.p3d";
		class TransportItems {
			MACRO_ADDITEM(PMC_Cap_Earpiece_Dark,1)
		};
	};
	class CLASS(Item_Cap_Earpiece_ION): Headgear_Base_F {
		MACRO_ITEM_COMMON
		author = "Optio";
		displayName = CSTRING(Cap_Earpiece_ION);
		model = "\A3\Weapons_F\DummyCap.p3d";
		class TransportItems {
			MACRO_ADDITEM(PMC_Cap_Earpiece_ION,1)
		};
	};
	
	class CLASS(Item_Cap_Backwards_Dark): Headgear_Base_F {
		MACRO_ITEM_COMMON
		author = "Spartan Tactical Group, Jonpas";
		displayName = CSTRING(Cap_Backwards_Dark);
		model = "\A3\Weapons_F\DummyCap.p3d";
		class TransportItems {
			MACRO_ADDITEM(PMC_Cap_Backwards_Dark,1)
		};
	};
	class CLASS(Item_Cap_Backwards_ION): Headgear_Base_F {
		MACRO_ITEM_COMMON
		author = "Optio";
		displayName = CSTRING(Cap_Backwards_ION);
		model = "\A3\Weapons_F\DummyCap.p3d";
		class TransportItems {
			MACRO_ADDITEM(PMC_Cap_Backwards_ION,1)
		};
	};
	
	class CLASS(Item_Helmet_CrewHeli_Skull): Headgear_Base_F {
		MACRO_ITEM_COMMON
		author = "Spartan Tactical Group";
		displayName = CSTRING(Helmet_CrewHeli_Skull);
		class TransportItems {
			MACRO_ADDITEM(PMC_Helmet_CrewHeli_Skull,1)
		};
	};
	
	
	class CLASS(Item_Cap_Headset_Dark): Headgear_Base_F {
		MACRO_ITEM_COMMON
		author = "Bohemia Interactive, Optio";
		displayName = CSTRING(Cap_Headset_Dark);
		model = "\A3\Weapons_F\DummyCap.p3d";
		class TransportItems {
			MACRO_ADDITEM(PMC_Cap_Headset_Dark,1)
		};
	};
	class CLASS(Item_Cap_Headset_ION): Headgear_Base_F {
		MACRO_ITEM_COMMON
		author = "Bohemia Interactive, Optio";
		displayName = CSTRING(Cap_Headset_ION);
		model = "\A3\Weapons_F\DummyCap.p3d";
		class TransportItems {
			MACRO_ADDITEM(PMC_Cap_Headset_ION,1)
		};
	};
};
