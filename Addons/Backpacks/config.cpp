class CfgPatches {
    class PMC_Backpacks {
        units[] = {
            "PMC_Backpack_AssaultExpanded_Black",
            "PMC_Backpack_AssaultExpanded_Green",
            "PMC_Backpack_AssaultExpanded_Tan",
            "PMC_Backpack_Carryall_DarkBlack",
            "PMC_Backpack_Kitbag_DarkBlack"
        };
        weapons[] = {};
        requiredVersion = 0.1;
        requiredAddons[] = {"PMC_Vests"};
        author = "Optio, zabb, Pomigit, BadHabitz, Jonpas, Rory";
    };
};

class CfgVehicles {
    class B_AssaultPack_Kerry;
    class PMC_Backpack_AssaultExpanded_Black: B_AssaultPack_Kerry {
        dlc = "PMC";
        scope = 2;
        author = "Optio, BadHabitz, Jonpas, Rory";
        displayname = "$STR_PMC_Backpacks_Backpack_AssaultExpanded_Black";
        picture = "\PMC\Addons\Backpacks\UI\backpack_assaultexpanded_black_ca.paa";
        hiddenSelectionsTextures[] = {
            "\a3\weapons_f\ammoboxes\bags\data\backpack_compact_blk_co.paa",
			"\PMC\Addons\Vests\data\vests_blk_co.paa"
        };
        maximumLoad = 180;
    };
    class PMC_Backpack_AssaultExpanded_Green: B_AssaultPack_Kerry {
        dlc = "PMC";
        scope = 2;
        author = "BadHabitz, Jonpas, Rory";
        displayname = "$STR_PMC_Backpacks_Backpack_AssaultExpanded_Green";
        picture = "\PMC\Addons\Backpacks\UI\backpack_assaultexpanded_green_ca.paa";
        hiddenSelectionsTextures[] = {
            "\a3\weapons_f\ammoboxes\bags\data\backpack_compact_rgr_co.paa",
			"\PMC\Addons\Vests\data\vests_grn_co.paa"
        };
        maximumLoad = 180;
    };
	/*
    class PMC_Backpack_AssaultExpanded_Khaki: B_AssaultPack_Kerry {
        dlc = "PMC";
        scope = 2;
        author = "BadHabitz, Jonpas, Rory";
        displayname = "$STR_PMC_Backpacks_Backpack_AssaultExpanded_Khaki";
        picture = "\PMC\Addons\Backpacks\UI\backpack_assaultexpanded_green_ca.paa";
        hiddenSelectionsTextures[] = {
            "\a3\weapons_f\ammoboxes\bags\data\backpack_compact_khk_co.paa",
			"\PMC\Addons\Vests\data\vests_khk_co.paa"
        };
        maximumLoad = 180;
    };*/
    class PMC_Backpack_AssaultExpanded_Coyote: B_AssaultPack_Kerry {
        dlc = "PMC";
        scope = 2;
        author = "BadHabitz, Jonpas, Rory";
        displayname = "$STR_PMC_Backpacks_Backpack_AssaultExpanded_Coyote";
        picture = "\PMC\Addons\Backpacks\UI\backpack_assaultexpanded_cbr_ca.paa";
        hiddenSelectionsTextures[] = {
            "\a3\weapons_f\ammoboxes\bags\data\backpack_compact_cbr_co.paa",
			"\PMC\Addons\Vests\data\vests_cbr_co.paa"
        };
        maximumLoad = 180;
    };

	
    class B_Carryall_Base;
    class PMC_Backpack_Carryall_DarkBlack: B_Carryall_Base {
        dlc = "PMC";
        scope = 2;
        author = "Bohemia Interactive, BadHabitz, Jonpas";
        displayName = "$STR_PMC_Backpacks_Backpack_Carryall_DarkBlack";
        picture = "\a3\Weapons_F\Ammoboxes\Bags\data\UI\icon_B_C_Tortila_blk.paa";
        hiddenSelectionsTextures[] = {"\a3\weapons_f\ammoboxes\bags\data\backpack_tortila_blk_co.paa"};
    };

    class B_Kitbag_Base;
    class PMC_Backpack_Kitbag_DarkBlack: B_Kitbag_Base {
        dlc = "PMC";
        scope = 2;
        author = "Bohemia Interactive, BadHabitz, Jonpas";
        displayName = "$STR_PMC_Backpacks_Backpack_Kitbag_DarkBlack";
        picture = "\PMC\Addons\Backpacks\UI\backpack_kitbag_darkblack_ca.paa";
        hiddenSelectionsTextures[] = {"\a3\weapons_f\ammoboxes\bags\data\backpack_fast_blk_co.paa"};
    };
/*

    // Filled
    class CLASS(Backpack_AssaultExpanded_Green_ExplosivesTechnician_Filled): CLASS(Backpack_AssaultExpanded_Green) {
        scope = 1;

        class TransportMagazines {
            MACRO_ADDMAGAZINE(APERSBoundingMine_Range_Mag,3)
            MACRO_ADDMAGAZINE(ClaymoreDirectionalMine_Remote_Mag,2)
            MACRO_ADDMAGAZINE(SLAMDirectionalMine_Wire_Mag,2)
            MACRO_ADDMAGAZINE(DemoCharge_Remote_Mag,1)
        };

        class TransportItems {
            MACRO_ADDITEM(ToolKit,1)
            MACRO_ADDITEM(MineDetector,1)
        };
    };

    class CLASS(Backpack_Kitbag_DarkBlack_Medic_Filled): CLASS(Backpack_Kitbag_DarkBlack) {
        scope = 1;

        class TransportItems {
            MACRO_ADDITEM(Medikit,1)
            MACRO_ADDITEM(FirstAidKit,10)
        };
    };

    class CLASS(Backpack_AssaultExpanded_Green_Specialist_Filled): CLASS(Backpack_AssaultExpanded_Green) {
        scope = 1;

        class TransportMagazines {
            MACRO_ADDMAGAZINE(200Rnd_65x39_cased_Box_Tracer,3)
        };
    };
	*/
};
