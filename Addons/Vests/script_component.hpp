#define COMPONENT vests
#include "\x\spartac\addons\main\script_mod.hpp"

#ifdef DEBUG_ENABLED_VEST
    #define DEBUG_MODE_FULL
#endif

#ifdef DEBUG_SETTINGS_VEST
    #define DEBUG_SETTINGS DEBUG_SETTINGS_VEST
#endif

#include "\x\spartac\addons\main\script_macros.hpp"
