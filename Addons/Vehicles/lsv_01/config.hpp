class B_PMC_LSV_01_armed_F: B_LSV_01_armed_F {
	author = "Bohemia Interactive";
	editorPreview = "\PMC\Addons\Vehicles\lsv_01\UI\prowler_armed.jpg";
	_generalMacro = "B_PMC_LSV_01_armed_F";
	scope = 2;
	scopeCurator = 2;
	DLC = "Expansion";
	side = 1;
	faction = "BLU_PMC_F";
	crew = "PMC_Unit_B_Contractor";
	typicalCargo[] = {};
	textureList[] = {"Black",1, "Olive",0, "Sand",0};
//	hiddenSelectionsTextures[] = {
//		"\A3\Soft_F_Exp\LSV_01\Data\NATO_LSV_01_black_CO.paa",
//		"\A3\Soft_F_Exp\LSV_01\Data\NATO_LSV_02_black_CO.paa",
//		"\A3\Soft_F_Exp\LSV_01\Data\NATO_LSV_03_black_CO.paa",
//		"\A3\Soft_F_Exp\LSV_01\Data\NATO_LSV_Adds_black_CO.paa"
//	};
};
class I_PMC_LSV_01_armed_F: B_PMC_LSV_01_armed_F {
	author = "Bohemia Interactive";
	_generalMacro = "I_PMC_LSV_01_armed_F";
	side = 2;
	faction = "IND_PMC_F";
	crew = "PMC_Unit_I_Contractor";
};

class B_PMC_LSV_01_unarmed_F: B_LSV_01_unarmed_F {
	author = "Bohemia Interactive";
	editorPreview = "\PMC\Addons\Vehicles\lsv_01\UI\prowler.jpg";
	_generalMacro = "B_PMC_LSV_01_unarmed_F";
	scope = 2;
	scopeCurator = 2;
	DLC = "Expansion";
	side = 1;
	faction = "BLU_PMC_F";
	crew = "PMC_Unit_B_Contractor";
	typicalCargo[] = {};
	textureList[] = {"Black",1, "Olive",0, "Sand",0};
//	hiddenSelectionsTextures[] = {
//		"\A3\Soft_F_Exp\LSV_01\Data\NATO_LSV_01_black_CO.paa",
//		"\A3\Soft_F_Exp\LSV_01\Data\NATO_LSV_02_black_CO.paa",
//		"\A3\Soft_F_Exp\LSV_01\Data\NATO_LSV_03_black_CO.paa",
//		"\A3\Soft_F_Exp\LSV_01\Data\NATO_LSV_Adds_black_CO.paa"
//	};
};
class I_PMC_LSV_01_unarmed_F: B_PMC_LSV_01_unarmed_F {
	author = "Bohemia Interactive";
	_generalMacro = "I_PMC_LSV_01_unarmed_F";
	side = 2;
	faction = "IND_PMC_F";
	crew = "PMC_Unit_I_Contractor";
};
