class Heli_Transport_02_base_F: Helicopter_Base_H {
	hiddenSelections[] = {
		"camo1",
		"camo2",
		"camo3"
	};
	class TextureSources {
		class Indep {
			displayName = "$STR_A3_TEXTURESOURCES_INDEP0";
			author = "$STR_A3_Bohemia_Interactive";
			textures[]= {
				"A3\Air_F_Beta\Heli_Transport_02\Data\Heli_Transport_02_1_INDP_CO.paa",
				"A3\Air_F_Beta\Heli_Transport_02\Data\Heli_Transport_02_2_INDP_CO.paa",
				"A3\Air_F_Beta\Heli_Transport_02\Data\Heli_Transport_02_3_INDP_CO.paa"
			};
			factions[] = {
				"IND_F"
			};
		};
		class Civil {
			displayName = "Dahoman";
			author = "$STR_A3_Bohemia_Interactive";
			textures[]= {
				"A3\Air_F_Beta\Heli_Transport_02\Data\skins\heli_transport_02_1_dahoman_co.paa",
				"A3\Air_F_Beta\Heli_Transport_02\Data\skins\heli_transport_02_2_dahoman_co.paa",
				"A3\Air_F_Beta\Heli_Transport_02\Data\skins\heli_transport_02_3_dahoman_co.paa"
			};
			factions[] = {
				"CIV_F"
			};
		};
		class Ion {
			displayName = "$STR_A3_TEXTURESOURCES_ION0";
			author = "Optio";
			textures[] = {
				"A3\Air_F_Beta\Heli_Transport_02\Data\skins\heli_transport_02_1_ion_co.paa",
				"A3\Air_F_Beta\Heli_Transport_02\Data\skins\heli_transport_02_2_ion_co.paa",
				"A3\Air_F_Beta\Heli_Transport_02\Data\skins\heli_transport_02_3_ion_co.paa"
			};
			factions[] = {
				"IND_PMC_F",
				"BLU_PMC_F"
			};
		};
	};
};

class I_Heli_Transport_02_F: Heli_Transport_02_base_F {
	dlc = "";
	textureList[] = {"Indep", 1};
};
class C_Heli_Transport_02_F: I_Heli_Transport_02_F {
	dlc = "PMC";
	faction = "CIV_F";
	editorPreview = "\PMC\Addons\Vehicles\heli_transport_02\UI\c_heli_transport_02_f.jpg";
	side = 3;
	scope = 2;
	crew = "C_man_1_1_F";
	_generalMacro="C_Heli_Transport_02_F";
	textureList[] = {"Civil", 1};
	hiddenSelectionsTextures[] = {
		"A3\Air_F_Beta\Heli_Transport_02\Data\skins\heli_transport_02_1_dahoman_co.paa",
		"A3\Air_F_Beta\Heli_Transport_02\Data\skins\heli_transport_02_2_dahoman_co.paa",
		"A3\Air_F_Beta\Heli_Transport_02\Data\skins\heli_transport_02_3_dahoman_co.paa"
	};
	weapons[] = {};
	magazines[] = {};
	incomingMissileDetectionSystem = 0;
};
class I_PMC_Heli_Transport_02_F: I_Heli_Transport_02_F {
	dlc = "PMC";
	faction = "IND_PMC_F";
	crew = "PMC_Unit_I_PilotHeli";
	editorPreview = "\PMC\Addons\Vehicles\heli_transport_02\UI\b_heli_transport_02_f.jpg";
	side = 2;
	scope = 2;
	_generalMacro="I_PMC_Heli_Transport_02_F";
	textureList[] = {"Ion", 1};
	hiddenSelectionsTextures[] = {
		"A3\Air_F_Beta\Heli_Transport_02\Data\skins\heli_transport_02_1_ion_co.paa",
		"A3\Air_F_Beta\Heli_Transport_02\Data\skins\heli_transport_02_2_ion_co.paa",
		"A3\Air_F_Beta\Heli_Transport_02\Data\skins\heli_transport_02_3_ion_co.paa"
	};
};
class B_PMC_Heli_Transport_02_F: I_PMC_Heli_Transport_02_F {
	faction = "BLU_PMC_F";
	crew = "PMC_Unit_B_PilotHeli";
	side = 1;
	scope = 2;
	_generalMacro="B_PMC_Heli_Transport_02_F";
	textureList[] = {"Ion", 1};
};
