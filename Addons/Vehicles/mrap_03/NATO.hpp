class I_MRAP_03_F: MRAP_03_base_F {
	textureList[] = {"Indep", 1};
};

class B_MRAP_03_F: I_MRAP_03_F {
	dlc = "PMC";
	author = "Diesel";
	displayName = "$STR_A3_CfgVehicles_MRAP_03_Base0";
	scope = 2;
	side = 1;
	faction = "BLU_F";
	_generalMacro="B_MRAP_03_F";
	editorPreview = "\PMC\Addons\Vehicles\mrap_03\UI\B_MRAP_03_F.jpg";
	textureList[] = {"Blufor", 1};
	crew = "B_soldier_F";
	typicalCargo[] = {"B_Soldier_lite_F"};
	class TransportItems
	{
		class _xx_FirstAidKit
		{
			name = "FirstAidKit";
			count = 8;
		};
	};
	class TransportMagazines{};
	class TransportWeapons{};
};


class I_MRAP_03_hmg_F: MRAP_03_hmg_base_F {
	textureList[] = {"Indep", 1};
};

class B_MRAP_03_hmg_F: I_MRAP_03_hmg_F {
	dlc = "PMC";
	author = "Diesel";
	displayName = "$STR_A3_CfgVehicles_MRAP_03_HMG_base0";
	_generalMacro="B_MRAP_03_hmg_F";
	scope = 2;
	side = 1;
	faction = "BLU_F";
	editorPreview = "\PMC\Addons\Vehicles\mrap_03\UI\B_MRAP_03_hmg_F.jpg";
	textureList[] = {"Blufor", 1};
	crew = "B_soldier_F";
	typicalCargo[] = {"B_Soldier_lite_F"};
	class TransportItems
	{
		class _xx_FirstAidKit
		{
			name = "FirstAidKit";
			count = 8;
		};
	};
	class TransportMagazines{};
	class TransportWeapons{};
};


class I_MRAP_03_gmg_F: MRAP_03_gmg_base_F {
	textureList[] = {"Indep", 1};
};

class B_MRAP_03_gmg_F: I_MRAP_03_gmg_F {
	dlc = "PMC";
	author = "Diesel";
	_generalMacro="B_MRAP_03_gmg_F";
	scope = 2;
	side = 1;
	faction = "BLU_F";
	editorPreview = "\PMC\Addons\Vehicles\mrap_03\UI\B_MRAP_03_gmg_F.jpg";
	textureList[] = {"Blufor", 1};
	crew = "B_soldier_F";
	typicalCargo[] = {"B_Soldier_lite_F"};
	class TransportItems
	{
		class _xx_FirstAidKit
		{
			name = "FirstAidKit";
			count = 8;
		};
	};
	class TransportMagazines{};
	class TransportWeapons{};
};

