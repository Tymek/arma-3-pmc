	dlc = "PMC";
	author = "Optio";
	scope = 2;
	side = 1;
	faction = "BLU_PMC_F";
	forceInGarage = 1;
	textureList[] = {"Blufor", 1};
	hiddenSelections[] = {"Camo1", "Camo2"};
	hiddenSelectionsTextures[] = {
		"\PMC\Addons\Vehicles\mrap_03\data\mrap_03_ext_co.paa",
		"\PMC\Addons\Vehicles\data\turret_co.paa"
	};
	hiddenSelectionsMaterials[] = {
		"\PMC\Addons\Vehicles\mrap_03\data\mrap_03_ext.rvmat",
		"\PMC\Addons\Vehicles\data\turret.rvmat"
	};
	class Damage {
		tex[]={};
		mat[]=
		{
			"PMC\Addons\Vehicles\mrap_03\data\MRAP_03_ext.rvmat",
			"A3\soft_f_beta\MRAP_03\Data\MRAP_03_ext_damage.rvmat",
			"A3\soft_f_beta\MRAP_03\Data\MRAP_03_ext_destruct.rvmat",
			"A3\soft_f_beta\MRAP_03\Data\MRAP_03_int.rvmat",
			"A3\soft_f_beta\MRAP_03\Data\MRAP_03_int_damage.rvmat",
			"A3\soft_f_beta\MRAP_03\Data\MRAP_03_int_destruct.rvmat",
			"PMC\Addons\Vehicles\data\turret.rvmat",
			"A3\Data_F\Vehicles\Turret_damage.rvmat",
			"A3\Data_F\Vehicles\Turret_destruct.rvmat",
			"A3\data_f\Glass_veh.rvmat",
			"A3\data_f\Glass_veh_armored_damage.rvmat",
			"A3\data_f\Glass_veh_armored_damage.rvmat",
			"A3\data_f\Glass_veh_int.rvmat",
			"A3\data_f\Glass_veh_armored_damage.rvmat",
			"A3\data_f\Glass_veh_armored_damage.rvmat"
		};
	};
	//crew = "B_soldier_F";
	//typicalCargo[] = {"B_Soldier_lite_F"};
	class TransportItems
	{
		class _xx_FirstAidKit
		{
			name = "FirstAidKit";
			count = 4;
		};
	};
	class TransportMagazines{};
	class TransportWeapons{};