class CfgWeapons {
	class Vest_NoCamo_Base;

	#define MACRO_PLATECARRIERFULL_COMMON \
		dlc = QUOTE(PREFIX); \
		scope = 2; \
		hiddenSelections[] = {"camo"}; \
		class ItemInfo: ItemInfo { \
			containerClass = "Supply120"; \
			mass = 60; \
			hiddenSelections[] = {"camo"}; \
			class HitpointsProtectionInfo { \
				class Chest { \
					hitpointName = "HitChest"; \
					armor = 16; \
					passThrough = 0.30000001; \
				}; \
				class Diaphragm { \
					hitpointName = "HitDiaphragm"; \
					armor = 16; \
					passThrough = 0.30000001; \
				}; \
				class Abdomen { \
					hitpointName = "HitAbdomen"; \
					armor = 16; \
					passThrough = 0.30000001; \
				}; \
				class Pelvis { \
					hitpointName = "HitPelvis"; \
					armor = 16; \
					passThrough = 0.30000001; \
				}; \
				class Body { \
					hitpointName = "HitBody"; \
					passThrough = 0.30000001; \
				}; \
			}; \
		};

	class V_PlateCarrierIAGL_oli;
	class V_PlateCarrierIA1_dgtl: Vest_NoCamo_Base {
		class ItemInfo;
	};
	class CLASS(Vest_PlateCarrierFull_Black): V_PlateCarrierIA1_dgtl {
		MACRO_PLATECARRIERFULL_COMMON
		author = "Optio";
		displayName = "$STR_PMC_Vests_Vest_PlateCarrierFull_Black";
		picture = QPATHTOF(UI\vest_platecarrierfull_black_ca.paa);
		//hiddenSelectionsTextures[] = {QPATHTOF(data\vest_platecarrierfull_black_co.paa)};
		hiddenSelectionsTextures[] = {QPATHTOF(data\equip_ia_vest01_blk_co.paa)};
	};
	class CLASS(Vest_PlateCarrierFull_Green): V_PlateCarrierIA1_dgtl {
		MACRO_PLATECARRIERFULL_COMMON
		author = "Pomigit, Jonpas";
		displayName = "$STR_PMC_Vests_Vest_PlateCarrierFull_Green";
		picture = QPATHTOF(UI\vest_platecarrierfull_green_ca.paa);
		//hiddenSelectionsTextures[] = {QPATHTOF(data\vest_platecarrierfull_green_co.paa)};
		//hiddenSelectionsTextures[] = {"\A3\Characters_F_Mark\INDEP\Data\equip_ia_vest01_oli_co.paa"};
		hiddenSelectionsTextures[] = {QPATHTOF(data\equip_ia_vest01_grn_co.paa)};
	};
	class CLASS(Vest_PlateCarrierFull_Olive): V_PlateCarrierIA1_dgtl {
		MACRO_PLATECARRIERFULL_COMMON
		author = "Pomigit, Jonpas";
		displayName = "$STR_PMC_Vests_Vest_PlateCarrierFull_Olive";
		picture = QPATHTOF(UI\vest_platecarrierfull_green_ca.paa);
		//hiddenSelectionsTextures[] = {QPATHTOF(data\vest_platecarrierfull_green_co.paa)};
		hiddenSelectionsTextures[] = {"\A3\Characters_F_Mark\INDEP\Data\equip_ia_vest01_oli_co.paa"};
	};
	class CLASS(Vest_PlateCarrierIA1_blk): V_PlateCarrierIAGL_oli {
		dlc = "PMC";
		scope = 2;
		author = "Optio";
		_generalMacro = "V_PlateCarrierIA1_blk";
		displayName = "$STR_PMC_Vests_Vest_PlateCarrierIAGL_Blk";
		//picture = QPATHTOF(UI\vest_platecarrierfull_green_ca.paa);
		//hiddenSelectionsTextures[] = {QPATHTOF(data\vest_platecarrierfull_green_co.paa)};
		hiddenSelectionsTextures[] = {
			"\PMC\Addons\Vests\data\equip_ia_vest01_blk_co.paa",
			"\PMC\Addons\Vests\data\ga_carrier_gl_rig_blk_co.paa"
		};
	};
	class CLASS(Vest_PlateCarrierIA1_grn): V_PlateCarrierIAGL_oli {
		dlc = "PMC";
		scope = 2;
		author = "Optio";
		_generalMacro = "V_PlateCarrierIA1_grn";
		displayName = "$STR_PMC_Vests_Vest_PlateCarrierIAGL_Grn";
		//picture = QPATHTOF(UI\vest_platecarrierfull_green_ca.paa);
		//hiddenSelectionsTextures[] = {QPATHTOF(data\vest_platecarrierfull_green_co.paa)};
		hiddenSelectionsTextures[] = {
			"\PMC\Addons\Vests\data\equip_ia_vest01_grn_co.paa",
			"\PMC\Addons\Vests\data\ga_carrier_gl_rig_grn_co.paa"
		};
	};
	

/*
	#define MACRO_PLATECARRIER_COMMON \
		dlc = QUOTE(PREFIX); \
		scope = 2; \
		hiddenSelections[] = {"camo"}; \
		class ItemInfo: ItemInfo { \
			hiddenSelections[] = {"camo"}; \
			class HitpointsProtectionInfo { \
				class Chest { \
					hitpointName = "HitChest"; \
					armor = 20; \
					passThrough = 0.5; \
				}; \
				class Diaphragm { \
					hitpointName = "HitDiaphragm"; \
					armor = 20; \
					passThrough = 0.5; \
				}; \
				class Abdomen { \
					hitpointName = "HitAbdomen"; \
					armor = 20; \
					passThrough = 0.5; \
				}; \
				class Body { \
					hitpointName = "HitBody"; \
					passThrough = 0.5; \
				}; \
			}; \
		};

	class V_PlateCarrier1_rgr: Vest_NoCamo_Base {
		class ItemInfo;
	};
	class CLASS(Vest_PlateCarrier_Black): V_PlateCarrier1_rgr {
		MACRO_PLATECARRIER_COMMON
		author = "Spartan Tactical Group, Pomigit, Jonpas, Rory";
		displayName = "$STR_PMC_Vests_Vest_PlateCarrier_Black";
		picture = QPATHTOF(UI\vest_platecarrier_black_ca.paa);
		hiddenSelectionsTextures[] = {QPATHTOF(data\vest_platecarrier_black_co.paa)};
	};
	class CLASS(Vest_PlateCarrier_Green): V_PlateCarrier1_rgr {
		MACRO_PLATECARRIER_COMMON
		author = "Spartan Tactical Group, Pomigit, Jonpas, Rory";
		displayName = "$STR_PMC_Vests_Vest_PlateCarrier_Green";
		picture = QPATHTOF(UI\vest_platecarrier_green_ca.paa);
		hiddenSelectionsTextures[] = {QPATHTOF(data\vest_platecarrier_green_co.paa)};
	};
	class CLASS(Vest_PlateCarrier_Coyote): V_PlateCarrier1_rgr {
		MACRO_PLATECARRIER_COMMON
		author = "Spartan Tactical Group, Pomigit, Jonpas, Rory";
		displayName = "$STR_PMC_Vests_Vest_PlateCarrier_Coyote";
		picture = QPATHTOF(UI\vest_platecarrier_coyote_ca.paa);
		hiddenSelectionsTextures[] = {QPATHTOF(data\vest_platecarrier_coyote_co.paa)};
	};
	class CLASS(Vest_PlateCarrier_Khaki): V_PlateCarrier1_rgr {
		MACRO_PLATECARRIER_COMMON
		author = "Spartan Tactical Group, Pomigit, Jonpas, Rory";
		displayName = "$STR_PMC_Vests_Vest_PlateCarrier_Khaki";
		picture = QPATHTOF(UI\vest_platecarrier_khaki_ca.paa);
		hiddenSelectionsTextures[] = {QPATHTOF(data\vest_platecarrier_khaki_co.paa)};
	};
	class CLASS(Vest_PlateCarrier_MARPAT): V_PlateCarrier1_rgr {
		MACRO_PLATECARRIER_COMMON
		author = "Spartan Tactical Group, Pomigit, Jonpas, Rory";
		displayName = "$STR_PMC_Vests_Vest_PlateCarrier_MARPAT";
		picture = QPATHTOF(UI\vest_platecarrier_marpat_ca.paa);
		hiddenSelectionsTextures[] = {QPATHTOF(data\vest_platecarrier_marpat_co.paa)};
	};
*/
	
	class V_Rangemaster_belt: Vest_NoCamo_Base{
		class ItemInfo;
	};
	class CLASS(Vest_RangeBelt_Black): V_Rangemaster_belt {
		dlc = QUOTE(PREFIX);
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Vests_Vest_RangeBelt_Black";
		picture = QPATHTOF(UI\vest_platecarrier_black_ca.paa);
		hiddenSelections[] = {"camo"};
		//hiddenSelectionsTextures[] = {QPATHTOF(data\vest_platecarrier_black_co.paa)};
		hiddenSelectionsTextures[] = {"\A3\Characters_F\BLUFOR\Data\vests_blk_co.paa"};

		class ItemInfo: ItemInfo {
			hiddenSelections[] = {"camo"};
		};
	};

	class Vest_Camo_Base;
	class V_TacVest_blk_POLICE: Vest_Camo_Base {
		class ItemInfo;
	};
	class CLASS(Vest_Tactical_DarkBlack): V_TacVest_blk_POLICE {
		dlc = QUOTE(PREFIX);
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Vests_Vest_Tactical_DarkBlack";
		picture = QPATHTOF(UI\vest_tactical_darkblack_ca.paa);
		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {QPATHTOF(data\tacticalvest_black_co.paa)};

		class ItemInfo: ItemInfo {
			hiddenSelections[] = {"camo"};
		};
	};
	
	class V_Press_F: Vest_Camo_Base {
		class ItemInfo;
	};
	class CLASS(Vest_VIP): V_Press_F {
		author = "Optio";
		_generalMacro = "V_VIP_F";
		scope = 2;
		//picture = "\A3\Characters_F_EPC\Data\UI\icon_V_PressVest_CA.paa";
		displayName = "Vest (VIP)";
		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {
			"\PMC\Addons\Vests\data\equip_vip_vest_01_co.paa"
		};
		descriptionShort="$STR_A3_SP_AL_II";
		class ItemInfo: ItemInfo {
			hiddenSelections[] = {"camo"};
			uniformModel="\A3\Characters_F_EPC\Civil\equip_press_vest_01.p3d";
			containerClass="Supply30";
			mass = 50;
			class HitpointsProtectionInfo {
				class Chest {
					hitpointName="HitChest";
					armor = 16;
					passThrough = 0.30000001;
				};
				class Diaphragm {
					hitpointName = "HitDiaphragm";
					armor = 16;
					passThrough = 0.30000001;
				};
				class Abdomen {
					hitpointName = "HitAbdomen";
					armor=16;
					passThrough = 0.30000001;
				};
				class Pelvis {
					hitpointName = "HitPelvis";
					armor=16;
					passThrough = 0.30000001;
				};
				class Body {
					hitpointName = "HitBody";
					passThrough = 0.30000001;
				};
			};
		};
	};
	
};
