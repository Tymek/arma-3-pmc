class CfgPatches {
	class PMC_Common {
		units[] = {};
		weapons[] = {};
		author = "Optio";
		requiredVersion = 1.62;
		requiredAddons[] = {};
        version = 0.3;
	};
};

#include "CfgMods.hpp"

#include "CfgFactionClasses.hpp"
