#define PREFIX PMC

#define MACRO_ITEM \
    dlc = QUOTE(PREFIX); \
	scope = 2; \
	scopeCurator = 2; \
	vehicleClass = "ItemsUniforms"; \
	model = "\A3\Weapons_F\DummySuitpack.p3d"; \
	author = "Spartan Tactical Group, Jonpas";

class CfgVehicles {
	class Item_Base_F;

	class PMC_Item_Uniform_Combat_LS_BS_GP_BB: Item_Base_F {
		displayName = "$STR_PMC_Units_Uniform_Combat_LS_BS_GP_BB";

		class TransportItems {
			class _xx_Uniform_Combat_LS_BS_GP_BB {
				name = PMC_Uniform_Combat_LS_BS_GP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Combat_RS_BS_GP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Combat_RS_BS_GP_BB";

		class TransportItems {
			class _xx_Uniform_Combat_RS_BS_GP_BB {
				name = PMC_Uniform_Combat_RS_BS_GP_BB;
				count = 1;
			};
		};
	};/*
	class PMC_Item_Uniform_Combat_LS_BS_GP_TB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Combat_LS_BS_GP_TB";

		class TransportItems {
			class _xx_Uniform_Combat_LS_BS_GP_TB {
				name = PMC_Uniform_Combat_LS_BS_GP_TB;
				count = 1;
			};
		};
	};*//*
	class PMC_Item_Uniform_Combat_RS_BS_GP_TB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Combat_RS_BS_GP_TB";

		class TransportItems {
			class _xx_Uniform_Combat_RS_BS_GP_TB {
				name = PMC_Uniform_Combat_RS_BS_GP_TB;
				count = 1;
			};
		};
	};*/
	class PMC_Item_Uniform_Combat_LS_BS_TP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Combat_LS_BS_TP_BB";

		class TransportItems {
			class _xx_Uniform_Combat_LS_BS_TP_BB {
				name = PMC_Uniform_Combat_LS_BS_TP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Combat_RS_BS_TP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Combat_RS_BS_TP_BB";

		class TransportItems {
			class _xx_Uniform_Combat_RS_BS_TP_BB {
				name = PMC_Uniform_Combat_RS_BS_TP_BB;
				count = 1;
			};
		};
	};/*
	class PMC_Item_Uniform_Combat_LS_BS_TP_TB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Combat_LS_BS_TP_TB";

		class TransportItems {
			class _xx_Uniform_Combat_LS_BS_TP_TB {
				name = PMC_Uniform_Combat_LS_BS_TP_TB;
				count = 1;
			};
		};
	};*//*
	class PMC_Item_Uniform_Combat_RS_BS_TP_TB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Combat_RS_BS_TP_TB";

		class TransportItems {
			class _xx_Uniform_Combat_RS_BS_TP_TB {
				name = PMC_Uniform_Combat_RS_BS_TP_TB;
				count = 1;
			};
		};
	};*/
	class PMC_Item_Uniform_Combat_LS_GS_BP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Combat_LS_GS_BP_BB";

		class TransportItems {
			class _xx_Uniform_Combat_LS_GS_BP_BB {
				name = PMC_Uniform_Combat_LS_GS_BP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Combat_RS_GS_BP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Combat_RS_GS_BP_BB";

		class TransportItems {
			class _xx_Uniform_Combat_RS_GS_BP_BB {
				name = PMC_Uniform_Combat_RS_GS_BP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Combat_LS_GS_TP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Combat_LS_GS_TP_BB";

		class TransportItems {
			class _xx_Uniform_Combat_LS_GS_TP_BB {
				name = PMC_Uniform_Combat_LS_GS_TP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Combat_RS_GS_TP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Combat_RS_GS_TP_BB";

		class TransportItems {
			class _xx_Uniform_Combat_RS_GS_TP_BB {
				name = PMC_Uniform_Combat_RS_GS_TP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Combat_LS_BS_DGP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Combat_LS_BS_DGP_BB";

		class TransportItems {
			class _xx_Uniform_Combat_LS_BS_DGP_BB {
				name = PMC_Uniform_Combat_LS_BS_DGP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Combat_RS_BS_DGP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Combat_RS_BS_DGP_BB";

		class TransportItems {
			class _xx_Uniform_Combat_RS_BS_DGP_BB {
				name = PMC_Uniform_Combat_RS_BS_DGP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Combat_LS_TS_BP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Combat_LS_TS_BP_BB";

		class TransportItems {
			class _xx_Uniform_Combat_LS_TS_BP_BB {
				name = PMC_Uniform_Combat_LS_TS_BP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Combat_RS_TS_BP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Combat_RS_TS_BP_BB";

		class TransportItems {
			class _xx_Uniform_Combat_RS_TS_BP_BB {
				name = PMC_Uniform_Combat_RS_TS_BP_BB;
				count = 1;
			};
		};
	};/*
	class PMC_Item_Uniform_Combat_LS_TS_GP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Combat_LS_TS_GP_BB";

		class TransportItems {
			class _xx_Uniform_Combat_LS_TS_GP_BB {
				name = PMC_Uniform_Combat_LS_TS_GP_BB;
				count = 1;
			};
		};
	};*//*
	class PMC_Item_Uniform_Combat_RS_TS_GP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Combat_RS_TS_GP_BB";

		class TransportItems {
			class _xx_Uniform_Combat_RS_TS_GP_BB {
				name = PMC_Uniform_Combat_RS_TS_GP_BB;
				count = 1;
			};
		};
	};*/
	class PMC_Item_Uniform_Combat_LS_TS_GP_TB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Combat_LS_TS_GP_TB";

		class TransportItems {
			class _xx_Uniform_Combat_LS_TS_GP_TB {
				name = PMC_Uniform_Combat_LS_TS_GP_TB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Combat_RS_TS_GP_TB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Combat_RS_TS_GP_TB";

		class TransportItems {
			class _xx_Uniform_Combat_RS_TS_GP_TB {
				name = PMC_Uniform_Combat_RS_TS_GP_TB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Combat_LS_CDBS_GP_TB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Combat_LS_CDBS_GP_TB";

		class TransportItems {
			class _xx_Uniform_Combat_LS_CDBS_GP_TB {
				name = PMC_Uniform_Combat_LS_CDBS_GP_TB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Combat_RS_CDBS_GP_TB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Combat_RS_CDBS_GP_TB";

		class TransportItems {
			class _xx_Uniform_Combat_RS_CDBS_GP_TB {
				name = PMC_Uniform_Combat_RS_CDBS_GP_TB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Combat_LS_CLBS_GP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Combat_LS_CLBS_GP_BB";

		class TransportItems {
			class _xx_Uniform_Combat_LS_CLBS_GP_BB {
				name = PMC_Uniform_Combat_LS_CLBS_GP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Combat_RS_CLBS_GP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Combat_RS_CLBS_GP_BB";

		class TransportItems {
			class _xx_Uniform_Combat_RS_CLBS_GP_BB {
				name = PMC_Uniform_Combat_RS_CLBS_GP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Combat_LS_CLRS_TP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Combat_LS_CLRS_TP_BB";

		class TransportItems {
			class _xx_Uniform_Combat_LS_CLRS_TP_BB {
				name = PMC_Uniform_Combat_LS_CLRS_TP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Combat_RS_CLRS_TP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Combat_RS_CLRS_TP_BB";

		class TransportItems {
			class _xx_Uniform_Combat_RS_CLRS_TP_BB {
				name = PMC_Uniform_Combat_RS_CLRS_TP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Combat_LS_CPS_BP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Combat_LS_CPS_BP_BB";

		class TransportItems {
			class _xx_Uniform_Combat_LS_CPS_BP_BB {
				name = PMC_Uniform_Combat_LS_CPS_BP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Combat_RS_CPS_BP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Combat_RS_CPS_BP_BB";

		class TransportItems {
			class _xx_Uniform_Combat_RS_CPS_BP_BB {
				name = PMC_Uniform_Combat_RS_CPS_BP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Garment_LS_BS_BP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Garment_LS_BS_BP_BB";

		class TransportItems {
			class _xx_Uniform_Garment_LS_BS_BP_BB {
				name = PMC_Uniform_Garment_LS_BS_BP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Garment_RS_BS_BP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Garment_RS_BS_BP_BB";

		class TransportItems {
			class _xx_Uniform_Garment_RS_BS_BP_BB {
				name = PMC_Uniform_Garment_RS_BS_BP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Garment_LS_BS_GP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Garment_LS_BS_GP_BB";

		class TransportItems {
			class _xx_Uniform_Garment_LS_BS_GP_BB {
				name = PMC_Uniform_Garment_LS_BS_GP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Garment_RS_BS_GP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Garment_RS_BS_GP_BB";

		class TransportItems {
			class _xx_Uniform_Garment_RS_BS_GP_BB {
				name = PMC_Uniform_Garment_RS_BS_GP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Garment_LS_OS_EP_TB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Garment_LS_OS_EP_TB";

		class TransportItems {
			class _xx_Uniform_Garment_LS_OS_EP_TB {
				name = PMC_Uniform_Garment_LS_OS_EP_TB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Garment_RS_OS_EP_TB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Garment_RS_OS_EP_TB";

		class TransportItems {
			class _xx_Uniform_Garment_RS_OS_EP_TB {
				name = PMC_Uniform_Garment_RS_OS_EP_TB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Garment_LS_GS_GP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Garment_LS_GS_GP_BB";

		class TransportItems {
			class _xx_Uniform_Garment_LS_GS_GP_BB {
				name = PMC_Uniform_Garment_LS_GS_GP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Garment_RS_GS_GP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Garment_RS_GS_GP_BB";

		class TransportItems {
			class _xx_Uniform_Garment_RS_GS_GP_BB {
				name = PMC_Uniform_Garment_RS_GS_GP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Garment_LS_GS_BP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Garment_LS_GS_BP_BB";

		class TransportItems {
			class _xx_Uniform_Garment_LS_GS_BP_BB {
				name = Uniform_Garment_LS_GS_BP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Garment_RS_GS_BP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Garment_RS_GS_BP_BB";

		class TransportItems {
			class _xx_Uniform_Garment_RS_GS_BP_BB {
				name = PMC_Uniform_Garment_RS_GS_BP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Garment_LS_GS_EP_TB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Garment_LS_GS_EP_TB";

		class TransportItems {
			class _xx_Uniform_Garment_LS_GS_EP_TB {
				name = PMC_Uniform_Garment_LS_GS_EP_TB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Garment_RS_GS_EP_TB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Garment_RS_GS_EP_TB";

		class TransportItems {
			class _xx_Uniform_Garment_RS_GS_EP_TB {
				name = PMC_Uniform_Garment_RS_GS_EP_TB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Garment_LS_ES_EP_TB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Garment_LS_ES_EP_TB";

		class TransportItems {
			class _xx_Uniform_Garment_LS_ES_EP_TB {
				name = PMC_Uniform_Garment_LS_ES_EP_TB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Garment_RS_ES_EP_TB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Garment_RS_ES_EP_TB";

		class TransportItems {
			class _xx_Uniform_Garment_RS_ES_EP_TB {
				name = PMC_Uniform_Garment_RS_ES_EP_TB;
				count = 1;
			};
		};
	};/*
	class PMC_Item_Uniform_Garment_LS_ES_BP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Garment_LS_ES_BP_BB";

		class TransportItems {
			class _xx_Uniform_Garment_LS_ES_BP_BB {
				name = PMC_Uniform_Garment_LS_ES_BP_BB;
				count = 1;
			};
		};
	};*//*
	class PMC_Item_Uniform_Garment_RS_ES_BP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Garment_RS_ES_BP_BB";

		class TransportItems {
			class _xx_Uniform_Garment_RS_ES_BP_BB {
				name = PMC_Uniform_Garment_RS_ES_BP_BB;
				count = 1;
			};
		};
	};*/
	class PMC_Item_Uniform_Garment_LS_ES_GP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Garment_LS_ES_GP_BB";

		class TransportItems {
			class _xx_Uniform_Garment_LS_ES_GP_BB {
				name = PMC_Uniform_Garment_LS_ES_GP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Garment_RS_ES_GP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Garment_RS_ES_GP_BB";

		class TransportItems {
			class _xx_Uniform_Garment_RS_ES_GP_BB {
				name = PMC_Uniform_Garment_RS_ES_GP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Garment_LS_TS_TP_TB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Garment_LS_TS_TP_TB";

		class TransportItems {
			class _xx_Uniform_Garment_LS_TS_TP_TB {
				name = PMC_Uniform_Garment_LS_TS_TP_TB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Garment_RS_TS_TP_TB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Garment_RS_TS_TP_TB";

		class TransportItems {
			class _xx_Uniform_Garment_RS_TS_TP_TB {
				name = PMC_Uniform_Garment_RS_TS_TP_TB;
				count = 1;
			};
		};
	};/*
	class PMC_Item_Uniform_Garment_LS_GS_TP_TB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Garment_LS_GS_TP_TB";

		class TransportItems {
			class _xx_Uniform_Garment_LS_GS_TP_TB {
				name = PMC_Uniform_Garment_LS_GS_TP_TB;
				count = 1;
			};
		};
	};*//*
	class PMC_Item_Uniform_Garment_RS_GS_TP_TB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Garment_RS_GS_TP_TB";

		class TransportItems {
			class _xx_Uniform_Garment_RS_GS_TP_TB {
				name = PMC_Uniform_Garment_RS_GS_TP_TB;
				count = 1;
			};
		};
	};*/
	class PMC_Item_Uniform_Garment_LS_TS_GP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Garment_LS_TS_GP_BB";

		class TransportItems {
			class _xx_Uniform_Garment_LS_TS_GP_BB {
				name = PMC_Uniform_Garment_LS_TS_GP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Garment_RS_TS_GP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Garment_RS_TS_GP_BB";

		class TransportItems {
			class _xx_Uniform_Garment_RS_TS_GP_BB {
				name = PMC_Uniform_Garment_RS_TS_GP_BB;
				count = 1;
			};
		};
	};/*
	class PMC_Item_Uniform_Garment_LS_OS_TP_TB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Garment_LS_OS_TP_TB";

		class TransportItems {
			class _xx_Uniform_Garment_LS_OS_TP_TB {
				name = PMC_Uniform_Garment_LS_OS_TP_TB;
				count = 1;
			};
		};
	};*//*
	class PMC_Item_Uniform_Garment_RS_OS_TP_TB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Garment_RS_OS_TP_TB";

		class TransportItems {
			class _xx_Uniform_Garment_RS_OS_TP_TB {
				name = PMC_Uniform_Garment_RS_OS_TP_TB;
				count = 1;
			};
		};
	};*/
	class PMC_Item_Uniform_Garment_LS_TS_BP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Garment_LS_TS_BP_BB";

		class TransportItems {
			class _xx_Uniform_Garment_LS_TS_BP_BB {
				name = PMC_Uniform_Garment_LS_TS_BP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Garment_RS_TS_BP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Garment_RS_TS_BP_BB";

		class TransportItems {
			class _xx_Uniform_Garment_RS_TS_BP_BB {
				name = PMC_Uniform_Garment_RS_TS_BP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Polo_TP_LS_TP_TB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Polo_TP_LS_TP_TB";

		class TransportItems {
			class _xx_Uniform_Polo_TP_LS_TP_TB {
				name = PMC_Uniform_Polo_TP_LS_TP_TB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Polo_TP_TS_GP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Polo_TP_TS_GP_BB";

		class TransportItems {
			class _xx_Uniform_Polo_TP_TS_GP_BB {
				name = PMC_Uniform_Polo_TP_TS_GP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Polo_TP_BS_TP_TB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Polo_TP_BS_TP_TB";

		class TransportItems {
			class _xx_Uniform_Polo_TP_BS_TP_TB {
				name = PMC_Uniform_Polo_TP_BS_TP_TB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Polo_TP_BS_LP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Polo_TP_BS_LP_BB";

		class TransportItems {
			class _xx_Uniform_Polo_TP_BS_LP_BB {
				name = PMC_Uniform_Polo_TP_BS_LP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Polo_TP_LS_GP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Polo_TP_LS_GP_BB";

		class TransportItems {
			class _xx_Uniform_Polo_TP_LS_GP_BB {
				name = PMC_Uniform_Polo_TP_LS_GP_BB;
				count = 1;
			};
		};
	};/*
	class PMC_Item_Uniform_Polo_TP_OS_TP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Polo_TP_OS_TP_BB";

		class TransportItems {
			class _xx_Uniform_Polo_TP_OS_TP_BB {
				name = PMC_Uniform_Polo_TP_OS_TP_BB;
				count = 1;
			};
		};
	};*/
	class PMC_Item_Uniform_Polo_TP_OS_LP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Polo_TP_OS_LP_BB";

		class TransportItems {
			class _xx_Uniform_Polo_TP_OS_LP_BB {
				name = PMC_Uniform_Polo_TP_OS_LP_BB;
				count = 1;
			};
		};
	};/*
	class PMC_Item_Uniform_Polo_TP_GS_TP_TB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Polo_TP_GS_TP_TB";

		class TransportItems {
			class _xx_Uniform_Polo_TP_GS_TP_TB {
				name = PMC_Uniform_Polo_TP_GS_TP_TB;
				count = 1;
			};
		};
	};*//*
	class PMC_Item_Uniform_Polo_TP_WS_TP_TB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Polo_TP_WS_TP_TB";

		class TransportItems {
			class _xx_Uniform_Polo_TP_WS_TP_TB {
				name = PMC_Uniform_Polo_TP_WS_TP_TB;
				count = 1;
			};
		};
	};*/
	class PMC_Item_Uniform_Polo_TP_WS_LP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Polo_TP_WS_LP_BB";

		class TransportItems {
			class _xx_Uniform_Polo_TP_WS_LP_BB {
				name = PMC_Uniform_Polo_TP_WS_LP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Polo_TP_WS_GP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Polo_TP_WS_GP_BB";

		class TransportItems {
			class _xx_Uniform_Polo_TP_WS_GP_BB {
				name = PMC_Uniform_Polo_TP_WS_GP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Polo_CP_LS_TP_OB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Polo_CP_LS_TP_OB";

		class TransportItems {
			class _xx_Uniform_Polo_CP_LS_TP_OB {
				name = PMC_Uniform_Polo_CP_LS_TP_OB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Polo_CP_RS_LP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Polo_CP_RS_LP_BB";

		class TransportItems {
			class _xx_Uniform_Polo_CP_RS_LP_BB {
				name = PMC_Uniform_Polo_CP_RS_LP_BB;
				count = 1;
			};
		};
	};/*
	class PMC_Item_Uniform_Polo_CP_BS_TP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Polo_CP_BS_TP_BB";

		class TransportItems {
			class _xx_Uniform_Polo_CP_BS_TP_BB {
				name = PMC_Uniform_Polo_CP_BS_TP_BB;
				count = 1;
			};
		};
	};*/
	class PMC_Item_Uniform_TShirt_JP_GS_LP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_TShirt_JP_GS_LP_BB";

		class TransportItems {
			class _xx_Uniform_TShirt_JP_GS_LP_BB {
				name = PMC_Uniform_TShirt_JP_GS_LP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_TShirt_JP_GS_TP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_TShirt_JP_GS_TP_BB";

		class TransportItems {
			class _xx_Uniform_TShirt_JP_GS_TP_BB {
				name = PMC_Uniform_TShirt_JP_GS_TP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_TShirt_JP_BS_LP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_TShirt_JP_BS_LP_BB";

		class TransportItems {
			class _xx_Uniform_TShirt_JP_BS_LP_BB {
				name = PMC_Uniform_TShirt_JP_BS_LP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_TShirt_JP_BS_TP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_TShirt_JP_BS_TP_BB";

		class TransportItems {
			class _xx_Uniform_TShirt_JP_BS_TP_BB {
				name = PMC_Uniform_TShirt_JP_BS_TP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_TShirt_JP_LS_LP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_TShirt_JP_LS_LP_BB";

		class TransportItems {
			class _xx_Uniform_TShirt_JP_LS_LP_BB {
				name = PMC_Uniform_TShirt_JP_LS_LP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_TShirt_JP_WS_LP_BB: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_TShirt_JP_WS_LP_BB";

		class TransportItems {
			class _xx_Uniform_TShirt_JP_WS_LP_BB {
				name = PMC_Uniform_TShirt_JP_WS_LP_BB;
				count = 1;
			};
		};
	};
	class PMC_Item_Uniform_Flightsuit_Black: Item_Base_F {
		MACRO_ITEM
		displayName = "$STR_PMC_Units_Uniform_Flightsuit_Black";

		class TransportItems {
			class _xx_Uniform_Flightsuit_Black {
				name = PMC_Uniform_Flightsuit_Black;
				count = 1;
			};
		};
	};
	
	class PMC_Item_Suit_VIP: Item_Base_F {
		dlc = "PMC";
		scope = 2;
		scopeCurator = 2;
		vehicleClass = "ItemsUniforms";
		model = "\A3\Weapons_F\DummySuitpack.p3d";
		author = "Spartan Tactical Group, Jonpas";
		displayName = "$STR_PMC_Units_Suit_VIP";
		class TransportItems {
			class _xx_Uniform_Flightsuit_Black {
				name = PMC_Suit_VIP;
				count = 1;
			};
		};
	};
	
	
#include "CfgVehicles_HelperUnits.hpp"
#include "CfgVehicles_PublicUnits.hpp"
};