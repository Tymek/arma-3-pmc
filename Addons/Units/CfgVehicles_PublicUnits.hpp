// Included in CfgVehicles.hpp

// Items
#define ITEMS_1(var) var
#define ITEMS_2(var) var, var
#define ITEMS_3(var) var, var, var
#define ITEMS_4(var) var, var, var, var
#define ITEMS_5(var) var, var, var, var, var
#define ITEMS_6(var) var, var, var, var, var, var
#define ITEMS_7(var) var, var, var, var, var, var, var
#define ITEMS_8(var) var, var, var, var, var, var, var, var
#define ITEMS_9(var) var, var, var, var, var, var, var, var, var
#define ITEMS_10(var) var, var, var, var, var, var, var, var, var, var
#define ITEMS_11(var) var, var, var, var, var, var, var, var, var, var, var
#define ITEMS_12(var) var, var, var, var, var, var, var, var, var, var, var, var

/*
class CLASS(Unit_I_Bodyguard): CLASS(Unit_Polo_CP_LS_TP_OB) {
    scope = 2;
    author = "Spartan Tactical Group, Pomigit, Jonpas";
    displayName = CSTRING(Unit_Bodyguard);
    weapons[] = {"Throw", "Put", "SMG_02_ACO_F", "hgun_P07_F", "Binocular"};
    respawnWeapons[] = {"Throw", "Put", "SMG_02_ACO_F", "hgun_P07_F", "Binocular"};
    magazines[] = {ITEMS_9(30Rnd_9x21_Mag), ITEMS_2(16Rnd_9x21_Mag), ITEMS_2(SmokeShell), ITEMS_2(HandGrenade)};
    respawnMagazines[] = {ITEMS_9(30Rnd_9x21_Mag), ITEMS_2(16Rnd_9x21_Mag), ITEMS_2(SmokeShell), ITEMS_2(HandGrenade)};
    linkedItems[] = {"G_Shades_Black", QCLASS(Vest_Tactical_DarkBlack)};
    respawnLinkedItems[] = {"G_Shades_Black", QCLASS(Vest_Tactical_DarkBlack)};

    headgearList[] = {
        QCLASS(Cap_Earpiece_BlackLogo), 1,
        QCLASS(Cap_Earpiece_TanLogo), 1
    };

    class EventHandlers: EventHandlers {
        init = "if (local (_this select 0)) then { [_this select 0, [], []] call BIS_fnc_unitHeadgear; };";
    };
};
class CLASS(Unit_B_Bodyguard): CLASS(Unit_I_Bodyguard) {
    side = 1;
    faction = BLU_PMC_F;
};*/

class CLASS(Unit_I_Contractor): CLASS(Unit_Polo_TP_LS_TP_TB) {
    scope = 2;
    author = "Spartan Tactical Group, Pomigit, Jonpas";
    displayName = "$STR_PMC_Units_Unit_Contractor";
    weapons[] = {
		"Throw",
		"Put",
		"PMC_rifle_01",
		"hgun_P07_F",
		"Binocular"
	};
    respawnWeapons[] = {
		"Throw",
		"Put",
		"PMC_rifle_01",
		"hgun_P07_F",
		"Binocular"
	};
    magazines[] = {
		ITEMS_4(30Rnd_556x45_Stanag),
		ITEMS_2(30Rnd_556x45_Stanag_Tracer_Yellow),
		"SmokeShell",
		"HandGrenade",
		ITEMS_2(MiniGrenade)
	};
    respawnMagazines[] = {
		ITEMS_4(30Rnd_556x45_Stanag),
		ITEMS_2(30Rnd_556x45_Stanag_Tracer_Yellow),
		"SmokeShell",
		"HandGrenade",
		ITEMS_2(MiniGrenade)
	};
    linkedItems[] = {
		"G_Shades_Black",
		"TAC_V_Sheriff_BA_TB2",
		"ItemMap", "ItemCompass", "ItemWatch", "ItemRadio"
	};
    respawnLinkedItems[] = {
		"G_Shades_Black",
		"TAC_V_Sheriff_BA_TB2",
		"ItemMap", "ItemCompass", "ItemWatch", "ItemRadio"
	};
	headgearProbability = 66;
    headgearList[] = {
		"H_Cap_blk_ION", 1,
		"PMC_Cap_Backwards_ION", 1,
		"PMC_Cap_Dark", 1,
		"PMC_Cap_Backwards_Dark", 1,
		"", 1
    };
	textSingular = "contractor";
	textPlural = "contractors";

    class EventHandlers: EventHandlers {
        init = "if (local (_this select 0)) then { [_this select 0, [], []] call BIS_fnc_unitHeadgear; };";
    };
};
class CLASS(Unit_B_Contractor): CLASS(Unit_I_Contractor) {
    scope = 2;
    side = 1;
    faction = "BLU_PMC_F";
};

class CLASS(Unit_I_Medic): CLASS(Unit_I_Contractor) {
	displayName = "$STR_PMC_Units_Unit_Medic";
    scope = 2;
    uniformClass = QCLASS(Uniform_Polo_TP_BS_LP_BB);
    hiddenSelectionsTextures[] = {QPATHTOF(data\uniform_polo_bs_lp_bb_co.paa)};
	backpack = "PMC_Backpack_AssaultExpanded_Black";
	items[] = {
		ITEMS_10(FirstAidKit),
		"Medikit"
	};
    respawnItems[] = {
		ITEMS_10(FirstAidKit),
		"Medikit"
	};
    linkedItems[] = {
		"TAC_V_Sheriff_BA_TB4",
		"ItemMap", "ItemCompass", "ItemWatch", "ItemRadio"
	};
	textSingular = "medic";
	textPlural = "medics";
	
	nameSound = "veh_infantry_medic_s";
	
	icon = "iconManMedic";
	role = "CombatLifeSaver";
};
class CLASS(Unit_B_Medic): CLASS(Unit_I_Medic) {
    scope = 2;
    side = 1;
    faction = "BLU_PMC_F";
};

class CLASS(Unit_I_Engineer): CLASS(Unit_I_Contractor) {
	displayName = "$STR_PMC_Units_Unit_Engineer";
    scope = 2;
    nakedUniform = QCLASS(Unit_Underwear_BlackLogo);
    model = "\A3\Characters_F\Civil\c_citizen5.p3d";
    hiddenSelections[] = {"Camo1", "Camo2"};
    uniformClass = QCLASS(Uniform_TShirt_JP_GS_TP_BB);
    hiddenSelectionsTextures[] = {
        QPATHTOF(data\underwear_green_co.paa),
        QPATHTOF(data\jeans_tp_bb_co.paa)
    };
	backpack = "PMC_Backpack_AssaultExpanded_Black";
	items[] = {
		"FirstAidKit",
		"ToolKit"
	};
    respawnItems[] = {
		"FirstAidKit",
		"ToolKit"
	};
    linkedItems[] = {
		"G_Shades_Black",
		"TAC_V_Sheriff_BA_TB6",
		"ItemMap", "ItemCompass", "ItemWatch", "ItemRadio"
	};
	
	icon = "iconManEngineer";
	engineer = 1;
	detectSkill = 60;
	canDeactivateMines = 1;
};
class CLASS(Unit_B_Engineer): CLASS(Unit_I_Engineer) {
    scope = 2;
    side = 1;
    faction = "BLU_PMC_F";
};


class CLASS(Unit_I_EOD): CLASS(Unit_I_Engineer) {
	displayName = "$STR_PMC_Units_Unit_EOD";
    scope = 2;
	items[] = {
		"FirstAidKit",
		"ToolKit",
		"MineDetector"
	};
    respawnItems[] = {
		"FirstAidKit",
		"ToolKit",
		"MineDetector"
	};
    nakedUniform = QCLASS(Unit_Underwear_BlackLogo);
    model = "\A3\Characters_F\Civil\c_citizen5.p3d";
    hiddenSelections[] = {"Camo1", "Camo2"};
    uniformClass = QCLASS(Uniform_TShirt_JP_BS_TP_BB);
    hiddenSelectionsTextures[] = {
        QPATHTOF(data\underwear_black_co.paa),
        QPATHTOF(data\jeans_tp_bb_co.paa)
    };
	
	canDeactivateMines = 1;
	detectSkill = 80;
	icon = "iconManExplosive";
	role = "Sapper";
};
class CLASS(Unit_B_EOD): CLASS(Unit_I_EOD) {
    scope = 2;
    side = 1;
    faction = "BLU_PMC_F";
};


/*
class CLASS(Unit_I_Contractor_GL): CLASS(Unit_Combat_RS_BS_GP_BB) {
    scope = 2;
    author = "Spartan Tactical Group, Pomigit, Jonpas";
    displayName = CSTRING(Unit_Contractor_GL);
    weapons[] = {"Throw", "Put", QCLASS(MX_GL_BlackCamo), "hgun_P07_F", "Binocular"};
    respawnWeapons[] = {"Throw", "Put", QCLASS(MX_BlackCamo), "hgun_P07_F", "Binocular"};
    magazines[] = {ITEMS_5(30Rnd_65x39_caseless_mag), "1Rnd_SmokeBlue_Grenade_shell", "1Rnd_SmokeGreen_Grenade_shell", "1Rnd_SmokeRed_Grenade_shell", ITEMS_3(1Rnd_HE_Grenade_shell), ITEMS_2(16Rnd_9x21_Mag), "SmokeShell", "SmokeShellGreen", ITEMS_2(HandGrenade)};
    respawnMagazines[] = {ITEMS_5(30Rnd_65x39_caseless_mag), "1Rnd_SmokeBlue_Grenade_shell", "1Rnd_SmokeGreen_Grenade_shell", "1Rnd_SmokeRed_Grenade_shell", ITEMS_3(1Rnd_HE_Grenade_shell), ITEMS_2(16Rnd_9x21_Mag), "SmokeShell", "SmokeShellGreen", ITEMS_2(HandGrenade)};
    linkedItems[] = {"G_Shades_Black", QCLASS(Vest_Tactical_DarkBlack)};
    respawnLinkedItems[] = {"G_Shades_Black", QCLASS(Vest_Tactical_DarkBlack)};

    headgearList[] = {
        QCLASS(Cap_BlackLogo), 1,
        QCLASS(Cap_TanLogo), 1,
        QCLASS(Cap_Earpiece_BlackLogo), 1,
        QCLASS(Cap_Earpiece_TanLogo), 1
    };

    class EventHandlers: EventHandlers {
        init = "if (local (_this select 0)) then { [_this select 0, [], []] call BIS_fnc_unitHeadgear; };";
    };
};
class CLASS(Unit_B_Contractor_GL): CLASS(Unit_I_Contractor_GL) {
    scope = 2;
    side = 1;
    faction = BLU_PMC_F;
};

class CLASS(Unit_I_Engineer): CLASS(Unit_Polo_TP_LS_TP_TB) {
    scope = 2;
    author = "Spartan Tactical Group, Pomigit, Jonpas";
    displayName = CSTRING(Unit_Engineer);
    weapons[] = {"Throw", "Put", QCLASS(MX_BlackCamo_MRCO), "hgun_P07_F", "Binocular"};
    respawnWeapons[] = {"Throw", "Put", QCLASS(MX_BlackCamo_MRCO), "hgun_P07_F", "Binocular"};
    magazines[] = {ITEMS_12(30Rnd_65x39_caseless_mag), ITEMS_2(16Rnd_9x21_Mag), ITEMS_2(SmokeShell), ITEMS_2(HandGrenade)};
    respawnMagazines[] = {ITEMS_12(30Rnd_65x39_caseless_mag), ITEMS_2(16Rnd_9x21_Mag), ITEMS_2(SmokeShell), ITEMS_2(HandGrenade)};
    linkedItems[] = {"G_Shades_Black", QCLASS(Vest_PlateCarrier_Green)};
    respawnLinkedItems[] = {"G_Shades_Black", QCLASS(Vest_PlateCarrier_Green)};
    backpack = QCLASS(Backpack_AssaultExpanded_Green_ExplosivesTechnician_Filled);

    engineer = 1;
    detectSkill = 80;
    canDeactivateMines = 1;
    icon = "iconManEngineer";
	picture = "pictureRepair";

    headgearList[] = {
        QCLASS(Cap_Backwards_BlackLogo), 1,
        QCLASS(Cap_Backwards_TanLogo), 1,
        QCLASS(Hat_Boonie_RangerGreen), 1,
        QCLASS(Hat_Boonie_DesertMARPAT), 1,
        QCLASS(Hat_Boonie_Woodland), 1,
        QCLASS(Hat_Boonie_UCP), 1
    };

    class EventHandlers: EventHandlers {
        init = "if (local (_this select 0)) then { [_this select 0, [], []] call BIS_fnc_unitHeadgear; };";
    };
};
class CLASS(Unit_B_Engineer): CLASS(Unit_I_Engineer) {
    scope = 2;
    side = 1;
    faction = BLU_PMC_F;
};

class CLASS(Unit_I_Medic): CLASS(Unit_Combat_RS_BS_GP_BB) {
    scope = 2;
    author = "Spartan Tactical Group, Pomigit, Jonpas";
    displayName = CSTRING(Unit_Medic);
    weapons[] = {"Throw", "Put", "SMG_02_ACO_F", "hgun_P07_F", "Binocular"};
    respawnWeapons[] = {"Throw", "Put", "SMG_02_ACO_F", "hgun_P07_F", "Binocular"};
    magazines[] = {ITEMS_9(30Rnd_9x21_Mag), ITEMS_2(16Rnd_9x21_Mag), "SmokeShell", ITEMS_2(HandGrenade)};
    respawnMagazines[] = {ITEMS_9(30Rnd_9x21_Mag), ITEMS_2(16Rnd_9x21_Mag), "SmokeShell", ITEMS_2(HandGrenade)};
    linkedItems[] = {"G_Shades_Black", QCLASS(Helmet_Ballistic_DarkBlack), QCLASS(Vest_Tactical_DarkBlack)};
    respawnLinkedItems[] = {"G_Shades_Black", QCLASS(Helmet_Ballistic_DarkBlack), QCLASS(Vest_Tactical_DarkBlack)};
    backpack = QCLASS(Backpack_Kitbag_DarkBlack_Medic_Filled);

    attendant = 1;
    icon = "iconManMedic";
    picture = "pictureHeal";

    class EventHandlers: EventHandlers {
        init = "if (local (_this select 0)) then { [_this select 0, [], []] call BIS_fnc_unitHeadgear; };";
    };
};
class CLASS(Unit_B_Medic): CLASS(Unit_I_Medic) {
    scope = 2;
    side = 1;
    faction = BLU_PMC_F;
};

*/
/*
class CLASS(Unit_I_Specialist): CLASS(Unit_Polo_TP_WS_GP_BB) {
    scope = 2;
    author = "Optio, Spartan Tactical Group, Pomigit, Jonpas";
    displayName = "$STR_PMC_Units_Unit_Specialist";
    weapons[] = {"Throw", "Put", "PMC_rifle_02", "hgun_Rook40_F", "Binocular"};
    respawnWeapons[] = {"Throw", "Put", "PMC_rifle_02", "hgun_Rook40_F", "Binocular"};
    magazines[] = {ITEMS_3(150Rnd_556x45_Drum_Mag_F), ITEMS_2(16Rnd_9x21_Mag), ITEMS_1(SmokeShell)};
    respawnMagazines[] = {ITEMS_3(150Rnd_556x45_Drum_Mag_F), ITEMS_2(16Rnd_9x21_Mag), ITEMS_1(SmokeShell)};
    linkedItems[] = {
		QCLASS(Vest_PlateCarrierFull_Black),
		"ItemMap", "ItemCompass", "ItemWatch", "ItemRadio"
	};
    respawnLinkedItems[] = {
		QCLASS(Vest_PlateCarrierFull_Black),
		"ItemMap", "ItemCompass", "ItemWatch", "ItemRadio"
	};
    //backpack = QCLASS(Backpack_AssaultExpanded_Green_Specialist_Filled);
	genericNames = "PMC";

    headgearList[] = {
		"H_Cap_blk_ION", 1,
		"PMC_Cap_Backwards_ION", 1,
		"PMC_Cap_Dark", 1,
		"PMC_Cap_Backwards_Dark", 1,
		"", 1
    };

    class EventHandlers: EventHandlers {
        init = "if (local (_this select 0)) then { [_this select 0, [], []] call BIS_fnc_unitHeadgear; };";
    };
};
class CLASS(Unit_B_Specialist): CLASS(Unit_I_Specialist) {
    scope = 2;
    side = 1;
    faction = BLU_PMC_F;
};
*/
class CLASS(Unit_I_TeamLeader): CLASS(Unit_Polo_TP_OS_LP_BB) {
    scope = 2;
    author = "Optio, Spartan Tactical Group, Pomigit, Jonpas";
    displayName = "$STR_PMC_Units_Unit_TeamLeader";
    weapons[] = {
		"Throw",
		"Put",
		"PMC_rifle_01_gl",
		"hgun_Rook40_F",
		"Binocular"
	};
    respawnWeapons[] = {
		"Throw", 
		"Put", 
		"PMC_rifle_01_gl", 
		"hgun_Rook40_F", 
		"Binocular"
	};
    magazines[] = {
		ITEMS_6(30Rnd_556x45_Stanag),
		ITEMS_2(30Rnd_556x45_Stanag_Tracer_Yellow),
		ITEMS_1(16Rnd_9x21_Mag),
		ITEMS_4(1Rnd_HE_Grenade_shell),
		ITEMS_2(SmokeShell)
	};
    respawnMagazines[] = {
		ITEMS_6(30Rnd_556x45_Stanag),
		ITEMS_2(30Rnd_556x45_Stanag_Tracer_Yellow),
		ITEMS_1(16Rnd_9x21_Mag),
		ITEMS_4(1Rnd_HE_Grenade_shell),
		ITEMS_2(SmokeShell)
	};
    linkedItems[] = {
		"G_Tactical_Black",
		"PMC_Cap_ION_hs",
		"TAC_V_Sheriff_BA_TB7",
		"ItemMap", "ItemCompass", "ItemWatch", "ItemRadio"
	};
    respawnLinkedItems[] = {
		"G_Tactical_Black",
		"PMC_Cap_blk_hs",
		"TAC_V_Sheriff_BA_TB7",
		"ItemMap", "ItemCompass", "ItemWatch", "ItemRadio"
	};

    icon = "iconManLeader";
    textSingular = "$STR_A3_nameSound_veh_infantry_officer_s";
    textPlural = "$STR_A3_nameSound_veh_infantry_officer_p";
    nameSound = "veh_infantry_officer_s";

    headgearList[] = {
		"PMC_Cap_ION_hs"
    };
	
    class SpeechVariants {
        class Default {
            speechSingular[] = {"veh_infantry_officer_s"};
            speechPlural[] = {"veh_infantry_officer_p"};
        };
    };
/*
    class EventHandlers: EventHandlers {
        init = "if (local (_this select 0)) then { [_this select 0, [], []] call BIS_fnc_unitHeadgear; };";
    };
	*/
};
class CLASS(Unit_B_TeamLeader): CLASS(Unit_I_TeamLeader) {
    scope = 2;
    side = 1;
    faction = "BLU_PMC_F";
};


class CLASS(Unit_I_SquadLeader): CLASS(Unit_I_TeamLeader) {
    displayName = "$STR_PMC_Units_Unit_SquadLeader";
    icon = "iconManOfficer";
    headgearList[] = {
		"PMC_MilCap_blk"
    };
    linkedItems[] = {
		"PMC_MilCap_blk",
		"PMC_Vest_Tactical_DarkBlack",
		"ItemMap", "ItemCompass", "ItemWatch", "ItemRadio"
	};
    respawnLinkedItems[] = {
		"PMC_MilCap_blk",
		"PMC_Vest_Tactical_DarkBlack",
		"ItemMap", "ItemCompass", "ItemWatch", "ItemRadio"
	};
    uniformClass = QCLASS(Uniform_Polo_TP_WS_GP_BB);
    hiddenSelectionsTextures[] = {QPATHTOF(data\uniform_polo_ws_gp_bb_co.paa)};
};

class CLASS(Unit_B_SquadLeader): CLASS(Unit_I_SquadLeader) {
    scope = 2;
    side = 1;
    faction = "BLU_PMC_F";
};

class CLASS(Unit_I_Marksman): CLASS(Unit_Polo_TP_LS_GP_BB) {
    scope = 2;
	role = "Marksman";
    author = "Optio, Spartan Tactical Group, Pomigit, Jonpas";
    displayName = "$STR_PMC_Units_Unit_Marksman";
    weapons[] = {
		"Throw",
		"Put",
		"PMC_rifle_03",
		"hgun_Rook40_F",
		"Rangefinder"
	};
    respawnWeapons[] = {
		"Throw",
		"Put",
		"PMC_rifle_03",
		"hgun_Rook40_F",
		"Rangefinder"
	};
    
	magazines[] = {
		ITEMS_6(20Rnd_762x51_Mag),
		ITEMS_2(16Rnd_9x21_Mag),
		ITEMS_2(SmokeShell),
		ITEMS_2(Chemlight_yellow)
	};
    respawnMagazines[] = {
		ITEMS_6(20Rnd_762x51_Mag),
		ITEMS_2(16Rnd_9x21_Mag),
		ITEMS_2(Chemlight_yellow),
		ITEMS_2(SmokeShell)
	};
	
    linkedItems[] = {
		"G_Shades_Blue", "H_Booniehat_khk",
		QCLASS(Vest_Tactical_DarkBlack),
		"ItemMap", "ItemCompass", "ItemWatch", "ItemRadio"
	};
    respawnLinkedItems[] = {
		"G_Shades_Blue", "H_Booniehat_khk",
		QCLASS(Vest_Tactical_DarkBlack),
		"ItemMap", "ItemCompass", "ItemWatch", "ItemRadio"
	};

    headgearList[] = {
		"H_Booniehat_khk", 1,
		"H_Booniehat_khk_hs", 1
    };
	
    class EventHandlers: EventHandlers {
        init = "if (local (_this select 0)) then { [_this select 0, [], []] call BIS_fnc_unitHeadgear; };";
    };
};
class CLASS(Unit_B_Marksman): CLASS(Unit_I_Marksman) {
    scope = 2;
    side = 1;
    faction = "BLU_PMC_F";
};

class CLASS(Unit_I_Sharpshooter): CLASS(Unit_Combat_LS_BS_TP_BB) {
    scope = 2;
	role = "Marksman";
    author = "Optio, Spartan Tactical Group, Pomigit, Jonpas";
    displayName = "$STR_PMC_Units_Unit_Sharpshooter";
    weapons[] = {"Throw", "Put", "srifle_EBR_SOS_bipod_snds_F", "hgun_Rook40_F", "Rangefinder"};
    respawnWeapons[] = {"Throw", "Put", "srifle_EBR_SOS_bipod_snds_F", "hgun_Rook40_F", "Rangefinder"};
    magazines[] = {
		ITEMS_5(20Rnd_762x51_Mag),
		ITEMS_2(16Rnd_9x21_Mag),
		ITEMS_2(SmokeShell),
		ITEMS_2(Chemlight_yellow)
	};
    respawnMagazines[] = {
		ITEMS_5(20Rnd_762x51_Mag),
		ITEMS_2(16Rnd_9x21_Mag),
		ITEMS_2(Chemlight_yellow),
		ITEMS_2(SmokeShell)
	};
	
    linkedItems[] = {
		"G_Tactical_Clear", "H_Watchcap_blk",
		"TAC_V_Sheriff_BA_TB",
		"ItemMap", "ItemCompass", "ItemWatch", "ItemRadio"
	};
    respawnLinkedItems[] = {
		"G_Tactical_Clear", "H_Watchcap_blk",
		"TAC_V_Sheriff_BA_TB",
		"ItemMap", "ItemCompass", "ItemWatch", "ItemRadio"
	};

    headgearList[] = {
		"H_Watchcap_blk", 1,
		"H_Watchcap_cbr", 1,
		"H_Watchcap_khk", 1,
		"H_Watchcap_camo", 1
    };
	
    class EventHandlers: EventHandlers {
        init = "if (local (_this select 0)) then { [_this select 0, [], []] call BIS_fnc_unitHeadgear; };";
    };
};
class CLASS(Unit_B_Sharpshooter): CLASS(Unit_I_Sharpshooter) {
    scope = 2;
    side = 1;
    faction = "BLU_PMC_F";
};

class CLASS(Unit_I_PilotHeli): CLASS(Unit_Flightsuit_Black) {
    scope = 2;
    author = "Spartan Tactical Group, Pomigit, Jonpas";
    displayName = "$STR_PMC_Units_Unit_PilotHeli";
    weapons[] = {"Throw", "Put", "hgun_Rook40_F", "Binocular"};
    respawnWeapons[] = {"Throw", "Put", "hgun_Rook40_F", "Binocular"};
    magazines[] = {ITEMS_2(16Rnd_9x21_Mag), "30Rnd_9x21_Yellow_Mag", ITEMS_2(SmokeShell)};
    respawnMagazines[] = {ITEMS_2(16Rnd_9x21_Mag), "30Rnd_9x21_Yellow_Mag", ITEMS_2(SmokeShell)};
    linkedItems[] = {"PMC_Cap_Headphones_Dark", "G_Aviator", QCLASS(Vest_Tactical_DarkBlack)};
    respawnLinkedItems[] = {"PMC_Cap_Headphones_Dark", "G_Aviator", QCLASS(Vest_Tactical_DarkBlack)};

    textSingular = "$STR_A3_nameSound_veh_infantry_pilot_s";
    textPlural = "$STR_A3_nameSound_veh_infantry_pilot_p";
    nameSound = "veh_infantry_pilot_s";

    class SpeechVariants {
        class Default {
            speechSingular[] = {"veh_infantry_pilot_s"};
            speechPlural[] = {"veh_infantry_pilot_p"};
        };
    };
	
    headgearList[] = {
		"PMC_Cap_Headphones_Dark"/*, 1,
		"H_PilotHelmetHeli_B", 0.1,
		"PMC_Helmet_CrewHeli_Skull", 0.05*/
    };
	
    class EventHandlers: EventHandlers {
        init = "if (local (_this select 0)) then { [_this select 0, [], []] call BIS_fnc_unitHeadgear; };";
    };
};
class CLASS(Unit_B_PilotHeli): CLASS(Unit_I_PilotHeli) {
    scope = 2;
    side = 1;
    faction = BLU_PMC_F;
};

// VIP
class Civilian_F;
class CLASS(Unit_I_VIP): Civilian_F {
    dlc = "PMC";
    scope = 2;
    scopeCurator = 2;
    side = 2;
    faction = "IND_PMC_F";
    author = "Spartan Tactical Group, Pomigit, Jonpas";
    displayname = "$STR_PMC_Units_Unit_VIP";
    model = QPATHTOF(data\vip.p3d);
    modelSides[] = {6};
    genericNames = QUOTE(PREFIX);
    uniformClass = QCLASS(Suit_VIP);

    weapons[] = {"Throw", "Put"};
    respawnWeapons[] = {"Throw", "Put"};
    magazines[] = {};
    respawnMagazines[] = {};
    items[] = {};
    respawnItems[] = {};
    linkedItems[] = {"G_Squares_Tinted"};
    respawnLinkedItems[] = {"G_Squares_Tinted"};
};
class CLASS(Unit_B_VIP): CLASS(Unit_I_VIP) {
    scope = 2;
    side = 1;
    faction = "BLU_PMC_F";
};
class CLASS(Unit_C_VIP): CLASS(Unit_I_VIP) {
    scope = 2;
    side = 3;
    faction = "CIV_F";
};
