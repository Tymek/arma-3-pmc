class Heli_Light_01_base_F: Helicopter_Base_H {
	hiddenSelections[] = {
		"camo1"
	};
	class TextureSources {
		class Blufor {
			displayName = "$STR_A3_TEXTURESOURCES_BLU0";
			author = "$STR_A3_Bohemia_Interactive";
			textures[]= {
				"A3\Air_F\Heli_Light_01\Data\Heli_Light_01_ext_Blufor_CO.paa"
			};
			factions[] = {
				"BLU_F"
			};
		};
		class Ion {
			displayName = "$STR_A3_TEXTURESOURCES_ION0";
			author = "Optio";
			textures[] = {
				"\a3\air_f\Heli_Light_01\Data\heli_light_01_ext_ion_co.paa"
			};
			factions[] = {
				"IND_PMC_F",
				"BLU_PMC_F"
			};
		};
	};
};

class B_Heli_Light_01_F: Heli_Light_01_unarmed_base_F {
	textureList[] = {"Blufor", 1};
};

class B_Heli_light_01_ion_F: B_Heli_Light_01_F {
	dlc = "PMC";
	faction = "BLU_PMC_F";
	crew = "PMC_Unit_B_PilotHeli";
	editorPreview = "\PMC\Addons\Vehicles\heli_light_01\UI\b_heli_light_01_armed_f.jpg";
	side = 1;
	scope = 2;
	textureList[] = {"Ion", 1};
	hiddenSelectionsTextures[] = {"\a3\air_f\Heli_Light_01\Data\heli_light_01_ext_ion_co.paa"};
};
class I_Heli_light_01_ion_F: B_Heli_light_01_ion_F {
	faction = "IND_PMC_F";
	crew = "PMC_Unit_I_PilotHeli";
	side = 2;
	scope = 2;
	textureList[] = {"Ion", 1};
};

class B_PMC_Heli_light_01_ion_F: C_Heli_light_01_ion_F {
	faction = "BLU_PMC_F";
	crew = "PMC_Unit_B_PilotHeli";
	side = 1;
	scope = 2;
};
class I_PMC_Heli_light_01_ion_F: C_Heli_light_01_ion_F {
	faction = "IND_PMC_F";
	crew = "PMC_Unit_I_PilotHeli";
	side = 2;
	scope = 2;
};
