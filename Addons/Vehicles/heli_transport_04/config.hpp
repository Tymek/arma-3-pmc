
class Pod_Heli_Transport_04_crewed_base_F;
class Land_Pod_Heli_Transport_04_bench_F: Pod_Heli_Transport_04_crewed_base_F {
	transportSoldier = 8;
};

/*
class I_PMC_Heli_Transport_04_F: O_Heli_Transport_04_F {
	dlc = "PMC";
	faction = "IND_PMC_F";
	crew = "PMC_Unit_I_PilotHeli";
	editorPreview = "\PMC\Addons\Vehicles\heli_transport_04\UI\taru.jpg";
	typicalCargo[] = {};
	side = 2;
	scope = 2;
	textureList[] = {"Black", 1};
	hiddenSelectionsTextures[] = {
		"A3\Air_F_Heli\Heli_Transport_04\Data\heli_transport_04_base_01_Black_co.paa",
		"A3\Air_F_Heli\Heli_Transport_04\Data\heli_transport_04_base_02_Black_co.paa"
	};
	class Turrets: Turrets {
		class LoadmasterTurret: LoadmasterTurret {
			gunnerType = "PMC_Unit_I_PilotHeli";
		};
	};
};
class B_PMC_Heli_Transport_04_F: I_PMC_Heli_Transport_04_F {
	faction = "BLU_PMC_F";
	crew = "PMC_Unit_B_PilotHeli";
	side = 1;
	scope = 2;
	class Turrets: Turrets {
		class LoadmasterTurret: LoadmasterTurret {
			gunnerType = "PMC_Unit_B_PilotHeli";
		};
	};
};

class O_Heli_Transport_04_ammo_F;
class I_PMC_Heli_Transport_04_ammo_F: O_Heli_Transport_04_ammo_F {
	dlc = "PMC";
	faction = "IND_PMC_F";
	crew = "PMC_Unit_I_PilotHeli";
	typicalCargo[] = {};
	editorPreview = "\PMC\Addons\Vehicles\heli_transport_04\UI\taru_ammo.jpg";
	side = 2;
	scope = 2;
	textureList[] = {"Black", 1};
	hiddenSelectionsTextures[] = {
		"A3\Air_F_Heli\Heli_Transport_04\Data\heli_transport_04_base_01_Black_co.paa",
		"A3\Air_F_Heli\Heli_Transport_04\Data\heli_transport_04_base_02_Black_co.paa",
		"A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_Pod_Ext01_Black_CO.paa",
		"A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_Pod_Ext02_Black_CO.paa"
	};
};
class B_PMC_Heli_Transport_04_ammo_F: I_PMC_Heli_Transport_04_ammo_F {
	faction = "BLU_PMC_F";
	crew = "PMC_Unit_B_PilotHeli";
	side = 1;
	scope = 2;
};

class O_Heli_Transport_04_bench_F;
class I_PMC_Heli_Transport_04_bench_F: O_Heli_Transport_04_bench_F {
	dlc = "PMC";
	faction = "IND_PMC_F";
	crew = "PMC_Unit_I_PilotHeli";
	typicalCargo[] = {};
	editorPreview = "\PMC\Addons\Vehicles\heli_transport_04\UI\taru_bench.jpg";
	side = 2;
	scope = 2;
	textureList[] = {"Black", 1};
	hiddenSelectionsTextures[] = {
		"A3\Air_F_Heli\Heli_Transport_04\Data\heli_transport_04_base_01_Black_co.paa",
		"A3\Air_F_Heli\Heli_Transport_04\Data\heli_transport_04_base_02_Black_co.paa",
		"A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_bench_Black_CO.paa"
	};
};
class B_PMC_Heli_Transport_04_bench_F: I_PMC_Heli_Transport_04_bench_F {
	faction = "BLU_PMC_F";
	crew = "PMC_Unit_B_PilotHeli";
	side = 1;
	scope = 2;
};

class O_Heli_Transport_04_box_F;
class I_PMC_Heli_Transport_04_box_F: O_Heli_Transport_04_box_F {
	dlc = "PMC";
	faction = "IND_PMC_F";
	crew = "PMC_Unit_I_PilotHeli";
	typicalCargo[] = {};
	editorPreview = "\PMC\Addons\Vehicles\heli_transport_04\UI\taru_cargo.jpg";
	side = 2;
	scope = 2;
	textureList[] = {"Black", 1};
	hiddenSelectionsTextures[] = {
		"A3\Air_F_Heli\Heli_Transport_04\Data\heli_transport_04_base_01_Black_co.paa",
		"A3\Air_F_Heli\Heli_Transport_04\Data\heli_transport_04_base_02_Black_co.paa",
		"A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_Pod_Ext01_Black_CO.paa",
		"A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_Pod_Ext02_Black_CO.paa"
	};
};
class B_PMC_Heli_Transport_04_box_F: I_PMC_Heli_Transport_04_box_F {
	faction = "BLU_PMC_F";
	crew = "PMC_Unit_B_PilotHeli";
	side = 1;
	scope = 2;
};

class O_Heli_Transport_04_covered_F;
class I_PMC_Heli_Transport_04_covered_F: O_Heli_Transport_04_covered_F {
	dlc = "PMC";
	faction = "IND_PMC_F";
	crew = "PMC_Unit_I_PilotHeli";
	typicalCargo[] = {};
	editorPreview = "\PMC\Addons\Vehicles\heli_transport_04\UI\taru_transport.jpg";
	side = 2;
	scope = 2;
	textureList[] = {"Black", 1};
	hiddenSelectionsTextures[] = {
		"A3\Air_F_Heli\Heli_Transport_04\Data\heli_transport_04_base_01_Black_co.paa",
		"A3\Air_F_Heli\Heli_Transport_04\Data\heli_transport_04_base_02_Black_co.paa",
		"A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_Pod_Ext01_Black_CO.paa",
		"A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_Pod_Ext02_Black_CO.paa"
	};
};
class B_PMC_Heli_Transport_04_covered_F: I_PMC_Heli_Transport_04_covered_F {
	faction = "BLU_PMC_F";
	crew = "PMC_Unit_B_PilotHeli";
	side = 1;
	scope = 2;
};

class O_Heli_Transport_04_fuel_F;
class I_PMC_Heli_Transport_04_fuel_F: O_Heli_Transport_04_fuel_F {
	dlc = "PMC";
	faction = "IND_PMC_F";
	crew = "PMC_Unit_I_PilotHeli";
	typicalCargo[] = {};
	editorPreview = "\PMC\Addons\Vehicles\heli_transport_04\UI\taru_fuel.jpg";
	side = 2;
	scope = 2;
	textureList[] = {"Black", 1};
	hiddenSelectionsTextures[] = {
		"A3\Air_F_Heli\Heli_Transport_04\Data\heli_transport_04_base_01_Black_co.paa",
		"A3\Air_F_Heli\Heli_Transport_04\Data\heli_transport_04_base_02_Black_co.paa",
		"A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_fuel_black_CO.paa"
	};
};
class B_PMC_Heli_Transport_04_fuel_F: I_PMC_Heli_Transport_04_fuel_F {
	faction = "BLU_PMC_F";
	crew = "PMC_Unit_B_PilotHeli";
	side = 1;
	scope = 2;
};

class O_Heli_Transport_04_medevac_F;
class I_PMC_Heli_Transport_04_medevac_F: O_Heli_Transport_04_medevac_F {
	dlc = "PMC";
	faction = "IND_PMC_F";
	crew = "PMC_Unit_I_PilotHeli";
	typicalCargo[] = {};
	editorPreview = "\PMC\Addons\Vehicles\heli_transport_04\UI\taru_medical.jpg";
	side = 2;
	scope = 2;
	textureList[] = {"Black", 1};
	hiddenSelectionsTextures[] = {
		"A3\Air_F_Heli\Heli_Transport_04\Data\heli_transport_04_base_01_Black_co.paa",
		"A3\Air_F_Heli\Heli_Transport_04\Data\heli_transport_04_base_02_Black_co.paa",
		"A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_Pod_Ext01_Black_CO.paa",
		"A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_Pod_Ext02_Black_CO.paa"
	};
};
class B_PMC_Heli_Transport_04_medevac_F: I_PMC_Heli_Transport_04_medevac_F {
	faction = "BLU_PMC_F";
	crew = "PMC_Unit_B_PilotHeli";
	side = 1;
	scope = 2;
};

class O_Heli_Transport_04_repair_F;
class I_PMC_Heli_Transport_04_repair_F: O_Heli_Transport_04_repair_F {
	dlc = "PMC";
	faction = "IND_PMC_F";
	crew = "PMC_Unit_I_PilotHeli";
	typicalCargo[] = {};
	editorPreview = "\PMC\Addons\Vehicles\heli_transport_04\UI\taru_repair.jpg";
	side = 2;
	scope = 2;
	textureList[] = {"Black", 1};
	hiddenSelectionsTextures[] = {
		"A3\Air_F_Heli\Heli_Transport_04\Data\heli_transport_04_base_01_Black_co.paa",
		"A3\Air_F_Heli\Heli_Transport_04\Data\heli_transport_04_base_02_Black_co.paa",
		"A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_Pod_Ext01_Black_CO.paa",
		"A3\Air_F_Heli\Heli_Transport_04\Data\Heli_Transport_04_Pod_Ext02_Black_CO.paa"
	};
};
class B_PMC_Heli_Transport_04_repair_F: I_PMC_Heli_Transport_04_repair_F {
	faction = "BLU_PMC_F";
	crew = "PMC_Unit_B_PilotHeli";
	side = 1;
	scope = 2;
};

//*/
/*
class Land_Pod_Heli_Transport_04_ammo_F;
class I_PMC_Land_Pod_Heli_Transport_04_ammo_F: Land_Pod_Heli_Transport_04_ammo_F {
	dlc = "PMC";
	faction = "IND_PMC_F";
	side = 2;
	scope = 2;
	textureList[] = {"Black", 1};
};
class B_PMC_Land_Pod_Heli_Transport_04_ammo_F: I_PMC_Land_Pod_Heli_Transport_04_ammo_F {
	faction = "BLU_PMC_F";
	side = 1;
	scope = 2;
};

*/


