class CfgPatches {
	class PMC_Vehicles {
		name = "Private Military Vehicles";
		author = "Optio";
		requiredAddons[] = {
			"PMC_Common",
			"PMC_Units",
			/*
			"A3_Armor_F_Beta_APC_Wheeled_01",
			"A3_Soft_F_Beta_MRAP_03",
			"A3_Armor_F",
			"A3_Armor_F_Beta",*/
			// Offroad
			"A3_Soft_F_Offroad_01",
			// Heli - Light
			"A3_Air_F_Heli_Light_01",
			// Heli - Transport
			"A3_Air_F_Beta_Heli_Transport_02",
			"A3_Air_F_Heli_Heli_Transport_04"
			
		};
        units[] = {
			"B_PMC_APC_Wheeled_01_cannon_F",
			"B_PMC_APC_Wheeled_01_unarmed_F",
			"B_PMC_APC_Wheeled_03_cannon_F",
			"B_PMC_LSV_01_unarmed_F",
			"B_PMC_LSV_01_armed_F",
			"I_PMC_APC_Wheeled_01_cannon_F",
			"I_PMC_APC_Wheeled_01_unarmed_F",
			"I_PMC_APC_Wheeled_03_cannon_F",
			"I_PMC_LSV_01_armed_F",
			"I_PMC_LSV_01_unarmed_F",
			/*
			"B_MRAP_03_F",
			"B_MRAP_03_hmg_F",
			"B_MRAP_03_gmg_F"*/
			// Offroad
			"B_PMC_Offroad_01_F",
			"I_PMC_Offroad_01_F",
			"B_PMC_Offroad_01_police_F",
			"I_PMC_Offroad_01_police_F",
			// Heli - Light
			"B_Heli_light_01_ion_F",
			"I_Heli_light_01_ion_F",
			
			// Heli - Transport
			"I_PMC_Heli_Transport_02_F",
			"B_PMC_Heli_Transport_02_F"
		};
        weapons[] = {};
		requiredVersion = 0.1;
	};
};

class CfgVehicles {
	class Air;
	class Helicopter: Air
	{
		class Turrets;
	};
	class Helicopter_Base_F: Helicopter
	{
		class Turrets: Turrets
		{
			class MainTurret;
		};
	};
	class Helicopter_Base_H: Helicopter_Base_F
	{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
				class ViewGunner;
			};
		};
	};
	class Heli_Transport_04_base_F: Helicopter_Base_H {
		class Turrets: Turrets
		{
			class MainTurret;
		};
	};
	class O_Heli_Transport_04_F: Heli_Transport_04_base_F {
		class Turrets: Turrets {
			class LoadmasterTurret;
		};
	};
	
	class B_APC_Wheeled_01_base_F;
	class B_APC_Wheeled_01_cannon_F: B_APC_Wheeled_01_base_F {
		class EventHandlers;
	};
	class B_LSV_01_armed_F;
	class B_LSV_01_unarmed_F;
	class I_APC_Wheeled_03_base_F;
	class I_APC_Wheeled_03_cannon_F: I_APC_Wheeled_03_base_F {
		class EventHandlers;
	};
	class C_Heli_light_01_ion_F;
	class Heli_Light_01_unarmed_base_F;
#include "apc_wheeled_01/config.hpp"
#include "apc_wheeled_03/config.hpp"
#include "heli_light_01/config.hpp"
#include "heli_transport_02/config.hpp"
#include "offroad_01/config.hpp"
#include "heli_transport_04/config.hpp"
#include "lsv_01/config.hpp"
};
	/*
	class APC_Wheeled_01_base_F: Wheeled_APC_F;
	class B_APC_Wheeled_01_base_F: APC_Wheeled_01_base_F;
	class B_APC_Wheeled_01_cannon_F: B_APC_Wheeled_01_base_F;*/
	
	
/*
	class B_APC_Wheeled_01_cannon_F;
	class Helicopter_Base_H;
	class Heli_Light_01_unarmed_base_F;
	class C_Heli_light_01_ion_F;
	class MRAP_03_base_F;
	class MRAP_03_hmg_base_F;
	class MRAP_03_gmg_base_F;
#include "apc_wheeled_01/config.hpp"
#include "mrap_03/config.hpp"
*/
