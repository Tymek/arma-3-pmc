class Offroad_01_civil_base_F;
class Offroad_01_ion_base_F: Offroad_01_civil_base_F {
	author = "Optio";
	_generalMacro = "Offroad_01_ion_base_F";
	typicalCargo[] = {};
	class TextureSources {
		class Ion {
			displayName = "$STR_A3_TEXTURESOURCES_ION0";
			author = "Optio";
			textures[] = {
				"\PMC\Addons\Vehicles\offroad_01\data\offroad_01_ext_blk_co.paa",
				"\PMC\Addons\Vehicles\offroad_01\data\offroad_01_ext_blk_co.paa"
			};
			factions[] = {
				"IND_PMC_F",
				"BLU_PMC_F",
				"CIV_F"
			};
		};
	};
};

class B_PMC_Offroad_01_F: Offroad_01_ion_base_F {
	dlc = "PMC";
	displayName = "Offroad (Black)";
	author = "Optio";
	_generalMacro = "B_PMC_Offroad_01_F";
	editorPreview = "\PMC\Addons\Vehicles\offroad_01\UI\offroad_01_f.jpg";
	scope = 2;
	scopeCurator = 2;
	side = 1;
	forceInGarage = 1;
	faction = "BLU_PMC_F";
	crew = "PMC_Unit_B_Contractor";
	textureList[] = {"Ion", 1};
	hiddenSelections[] = {
		"camo",
		"camo2"
	};
	hiddenSelectionsTextures[] = {
		"\PMC\Addons\Vehicles\offroad_01\data\offroad_01_ext_blk_co.paa",
		"\PMC\Addons\Vehicles\offroad_01\data\offroad_01_ext_blk_co.paa"
	};
	animationList[] = {
		"HidePolice", 1,
		"HideBumper1", 1,
		"HideBumper2", 0,
		"HideConstruction", 0.25,
		"HideDoor3", 0.2,
		"HideBackpacks", 0.1
	};
};
class I_PMC_Offroad_01_F: B_PMC_Offroad_01_F {
	_generalMacro = "I_PMC_Offroad_01_F";
	crew = "PMC_Unit_I_Contractor";
	faction = "IND_PMC_F";
	side = 2;
	forceInGarage = 0;
};

class B_PMC_Offroad_01_police_F: B_PMC_Offroad_01_F {
	displayName="Offroad Police";
	_generalMacro = "B_PMC_Offroad_01_police_F";
	editorPreview = "\PMC\Addons\Vehicles\offroad_01\UI\offroad_01_pl_f.jpg";
	side = 1;
	faction = "BLU_PMC_F";
	forceInGarage = 0;
	animationList[] = {
		"HidePolice", 0,
		"HideBumper1", 1,
		"HideBumper2", 0,
		"HideConstruction", 0.1,
		"HideDoor3", 0,
		"HideBackpacks", 1
	};
};

class I_PMC_Offroad_01_PL_F: B_PMC_Offroad_01_police_F {
	side = 2;
	crew = "PMC_Unit_I_Contractor";
	faction = "IND_PMC_F";
	_generalMacro = "I_PMC_Offroad_01_PL_F";
};
