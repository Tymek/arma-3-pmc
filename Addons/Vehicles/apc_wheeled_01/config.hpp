class B_PMC_APC_Wheeled_01_cannon_F: B_APC_Wheeled_01_cannon_F {
	dlc = "PMC";
	displayName="AMV-7 Marshall (Black)";
	author = "Optio";
	editorPreview = "\PMC\Addons\Vehicles\apc_wheeled_01\UI\apc_wheeled_01_cannon_f.jpg";
	scope = 2;
	side = 1;
	faction = "BLU_PMC_F";
	forceInGarage = 1;
	hiddenSelectionsMaterials[] = {
		"PMC\Addons\Vehicles\apc_wheeled_01\data\apc_wheeled_01_base.rvmat",
		"PMC\Addons\Vehicles\apc_wheeled_01\data\apc_wheeled_01_adds.rvmat",
		"PMC\Addons\Vehicles\apc_wheeled_01\data\apc_wheeled_01_tows.rvmat"
	};
	hiddenSelectionsTextures[] = {
		"PMC\Addons\Vehicles\apc_wheeled_01\data\apc_wheeled_01_base_co.paa",
		"PMC\Addons\Vehicles\apc_wheeled_01\data\apc_wheeled_01_adds_co.paa",
		"PMC\Addons\Vehicles\apc_wheeled_01\data\apc_wheeled_01_tows_co.paa"
	};
	class Damage {
		tex[] = {};
		mat[] = {
			"PMC\Addons\Vehicles\apc_wheeled_01\data\apc_wheeled_01_adds.rvmat",
			"a3\armor_f_beta\APC_Wheeled_01\Data\APC_Wheeled_01_adds_damage.rvmat",
			"a3\armor_f_beta\APC_Wheeled_01\Data\APC_Wheeled_01_adds_destruct.rvmat",
			"PMC\Addons\Vehicles\apc_wheeled_01\data\apc_wheeled_01_base.rvmat",
			"a3\armor_f_beta\APC_Wheeled_01\Data\APC_Wheeled_01_base_damage.rvmat",
			"a3\armor_f_beta\APC_Wheeled_01\Data\APC_Wheeled_01_base_destruct.rvmat",
			"PMC\Addons\Vehicles\apc_wheeled_01\data\apc_wheeled_01_tows.rvmat",
			"a3\armor_f_beta\APC_Wheeled_01\Data\APC_Wheeled_01_tows_damage.rvmat",
			"a3\armor_f_beta\APC_Wheeled_01\Data\APC_Wheeled_01_tows_destruct.rvmat",
			"a3\armor_f_beta\APC_Wheeled_01\Data\APC_Wheeled_01_int.rvmat",
			"a3\armor_f_beta\APC_Wheeled_01\Data\APC_Wheeled_01_int_damage.rvmat",
			"a3\armor_f_beta\APC_Wheeled_01\Data\APC_Wheeled_01_int_destruct.rvmat"
		};
	};
	class EventHandlers: EventHandlers {};
	class ViewOptics;
	class AnimationSources;
	class Turrets;/*: Turrets
	class MainTurret: MainTurret
		class ViewGunner;
		class Turrets: Turrets
			class CommanderOptics: CommanderOptics
				class ViewGunner: ViewGunner
				class ViewOptics: ViewOptics
				class OpticsIn: Optics_Commander_01
					class Wide: Wide
					class Medium: Medium
					class Narrow: Narrow
				class HitPoints
					class HitTurret
					class HitGun
		class ViewOptics: RCWSOptics
		class OpticsIn: Optics_Gunner_APC_01
			class Wide: Wide
			class Medium: Medium
			class Narrow: Narrow
		class HitPoints
			class HitTurret
			class HitGun
	*/
};
class I_PMC_APC_Wheeled_01_cannon_F: B_PMC_APC_Wheeled_01_cannon_F {
	faction = "IND_PMC_F";
	side = 2;
	forceInGarage = 0;
};

/**
 * Unarmed. I did it!
 */
class B_PMC_APC_Wheeled_01_unarmed_F: B_PMC_APC_Wheeled_01_cannon_F {
	displayName="AMV-7 Marshall Unarmed";
	author = "Optio";
	editorPreview = "\PMC\Addons\Vehicles\apc_wheeled_01\UI\apc_wheeled_01_unarmed_f.jpg";
	forceInGarage = 1;
	driverIsCommander = true;
	delete ViewOptics;
	/*
	class Turrets: Turrets {
		class MainTurret: MainTurret {
			delete ViewGunner;
			class Turrets: Turrets {
				class CommanderOptics: CommanderOptics {
					delete ViewGunner;
					delete ViewOptics;
					class OpticsIn: Optics_Commander_01 {
						delete Wide;
						delete Medium;
						delete Narrow;
					};
					class HitPoints {
						delete HitTurret;
						delete HitGun;
					};
				};
			};
			class ViewOptics;
			class OpticsIn: Optics_Gunner_APC_01 {
				delete Wide;
				delete Medium;
				delete Narrow;
			};
			class HitPoints {
				delete HitTurret;
				delete HitGun;
			};
		};
	};*/
	
	class EventHandlers: EventHandlers { // Lock gunner and commander
		init="if (local (_this select 0)) then {(_this select 0) lockTurret [[0],true]; (_this select 0) lockTurret [[0,0],true]; };";
	};
	class AnimationSources: AnimationSources
	{
		delete muzzle_rot;
		delete muzzle_hide;
		class HideTurret
		{
			source = "user";
			initPhase = 1;
			animPeriod = 0.001;
			mass = -10000;
		};
	};
	
	maxSpeed = 125; // 110
	fuelCapacity = 50; // 45
	waterLeakiness = 3; // 2.5
	antiRollbarForceCoef = 15; // 24
	antiRollbarForceLimit = 25; // 30
	antiRollbarSpeedMin = 15; // 15
	antiRollbarSpeedMax = 55; //65
	animationList[] = {
		"HideTurret", 1
	};
};

class I_PMC_APC_Wheeled_01_unarmed_F: B_PMC_APC_Wheeled_01_unarmed_F {
	faction = "IND_PMC_F";
	side = 2;
	forceInGarage = 0;
};


