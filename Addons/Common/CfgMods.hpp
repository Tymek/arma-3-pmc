class CfgMods
{
	class Mod_Base;
	class PMC: Mod_Base
	{
		name = "Private Military";
		author = "Optio";
		dir = "@PMC";
		picture = "PMC\Addons\Common\data\logo_ion_ca.paa";
		logo = "PMC\Addons\Common\data\logo_ion_small_ca.paa";
		logoOver = "PMC\Addons\Common\data\logo_ion_small_ca.paa";
		logoSmall = "PMC\Addons\Common\data\logo_ion_small_ca.paa";
        hidePicture = "true";
        hideName = "true";
        actionName = "WEBSITE";
        action = "";
		description = "Private Military assets for Arma 3";
		tooltipOwned = "ION Inc.";
		dlcColor[] = {0.5,0.5,0.5,1};
	};
};
