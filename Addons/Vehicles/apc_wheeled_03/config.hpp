class B_PMC_APC_Wheeled_03_cannon_F: I_APC_Wheeled_03_cannon_F {
	dlc = "PMC";
	displayName="AFV-4 Gorgon (Green)";
	author = "Optio";
	editorPreview = "\PMC\Addons\Vehicles\apc_wheeled_03\UI\apc_wheeled_03_cannon_f.jpg";
	scope = 2;
	side = 1;
	faction = "BLU_PMC_F";
	forceInGarage = 1;
	hiddenSelectionsTextures[] = {
		"PMC\Addons\Vehicles\apc_wheeled_03\data\apc_wheeled_03_ext_grn_co.paa",
		"PMC\Addons\Vehicles\apc_wheeled_03\data\apc_wheeled_03_ext2_grn_co.paa",
		"PMC\Addons\Vehicles\apc_wheeled_03\data\rcws30_grn_co.paa",
		"PMC\Addons\Vehicles\apc_wheeled_03\data\apc_wheeled_03_ext_alpha_grn_co.paa"
	};
	class AnimationSources;
	class EventHandlers: EventHandlers {};
};

class I_PMC_APC_Wheeled_03_cannon_F: B_PMC_APC_Wheeled_03_cannon_F {
	faction = "IND_PMC_F";
	side = 2;
	forceInGarage = 0;
};


class B_PMC_APC_Wheeled_03_unarmed_F: B_PMC_APC_Wheeled_03_cannon_F {
	displayName="AFV-4 Gorgon Unarmed";
	editorPreview = "\PMC\Addons\Vehicles\apc_wheeled_03\UI\apc_wheeled_03_unarmed_f.jpg";
	author = "Optio";
	forceInGarage = 1;
	class AnimationSources: AnimationSources {
		class HideTurret {
			source = "user";
			initPhase = 1;
			animPeriod = 0.001;
			mass = -10000;
		};
	};
	animationList[] = {
		"HideTurret", 1
	};
	class EventHandlers: EventHandlers { // Lock gunner and commander
		init="if (local (_this select 0)) then {(_this select 0) lockTurret [[0],true]; (_this select 0) lockTurret [[0,0],true]; };";
	};
	maxSpeed = 125;
	waterLeakiness = 3;
};
class I_PMC_APC_Wheeled_03_unarmed_F: B_PMC_APC_Wheeled_03_unarmed_F {
	faction = "IND_PMC_F";
	side = 2;
	forceInGarage = 0;
};
