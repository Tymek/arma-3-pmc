#define PREFIX PMC

#define DOUBLES(var1,var2) var1##_##var2
#define QUOTE(var1) #var1

#define CLASS(var1) DOUBLES(PREFIX,var1)
#define QCLASS(var1) QUOTE(DOUBLES(PREFIX,var1))

#define QPATHTOF(var1) QUOTE(\PREFIX\Addons\Vests\##var1)

class CfgPatches {
    class PMC_Vests {
        units[] = {
            QCLASS(Item_Vest_PlateCarrierFull_Black),
            QCLASS(Item_Vest_PlateCarrierFull_Olive),
            QCLASS(Item_Vest_PlateCarrierFull_Green),/*
            QCLASS(Item_Vest_PlateCarrier_Black),
            QCLASS(Item_Vest_PlateCarrier_Green),
            QCLASS(Item_Vest_PlateCarrier_Coyote),
            QCLASS(Item_Vest_PlateCarrier_Khaki),
            QCLASS(Item_Vest_PlateCarrier_MARPAT),*/
			QCLASS(Item_Vest_RangeBelt_Black),
            QCLASS(Item_Vest_Tactical_DarkBlack),
			QCLASS(Item_Vest_PlateCarrierIA1_blk),
			QCLASS(Item_Vest_PlateCarrierIA1_grn),
			QCLASS(Item_Vest_VIP)
        };
        weapons[] = {
            QCLASS(Vest_PlateCarrierFull_Black),
            QCLASS(Vest_PlateCarrierFull_Olive),
            QCLASS(Vest_PlateCarrierFull_Green),/*
            QCLASS(Vest_PlateCarrier_Black),
            QCLASS(Vest_PlateCarrier_Green),
            QCLASS(Vest_PlateCarrier_Coyote),
            QCLASS(Vest_PlateCarrier_Khaki),
            QCLASS(Vest_PlateCarrier_MARPAT),*/
			QCLASS(Vest_RangeBelt_Black),
            QCLASS(Vest_Tactical_DarkBlack),
			QCLASS(Vest_PlateCarrierIA1_blk),
			QCLASS(Vest_PlateCarrierIA1_grn),
			QCLASS(Vest_VIP)
        };
        requiredVersion = 0.1;
        requiredAddons[] = {
			"PMC_Common",
			"A3_Characters_F"
		};
        author = "Optio, Jonpas, Pomigit, BadHabitz, Rory";
    };
};

#include "CfgVehicles.hpp"
#include "CfgWeapons.hpp"
