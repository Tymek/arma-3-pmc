class CfgWeapons {
	class Uniform_Base;
	class UniformItem;
	class PMC_Uniform_Base: Uniform_Base {
		dlc = "PMC";
		scope = 0;
		model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
		
		class ItemInfo: UniformItem {
			uniformModel = "-";
			containerClass = "Supply40";
			mass = 40;
		};
	};

	class PMC_Uniform_Combat_LS_BS_GP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Combat_LS_BS_GP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_combat_ls_bs_gp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Combat_LS_BS_GP_BB";
		};
	};
	class PMC_Uniform_Combat_RS_BS_GP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Combat_RS_BS_GP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_combat_rs_bs_gp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Combat_RS_BS_GP_BB";
		};
	};/*
	class PMC_Uniform_Combat_LS_BS_GP_TB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Combat_LS_BS_GP_TB";
		picture = "\PMC\Addons\Units\UI\uniform_combat_ls_bs_gp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Combat_LS_BS_GP_TB";
		};
	};*//*
	class PMC_Uniform_Combat_RS_BS_GP_TB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Combat_RS_BS_GP_TB";
		picture = "\PMC\Addons\Units\UI\uniform_combat_rs_bs_gp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Combat_RS_BS_GP_TB";
		};
	};*/
	class PMC_Uniform_Combat_LS_BS_TP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Combat_LS_BS_TP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_combat_ls_bs_tp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Combat_LS_BS_TP_BB";
		};
	};
	class PMC_Uniform_Combat_RS_BS_TP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Combat_RS_BS_TP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_combat_rs_bs_tp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Combat_RS_BS_TP_BB";
		};
	};/*
	class PMC_Uniform_Combat_LS_BS_TP_TB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Combat_LS_BS_TP_TB";
		picture = "\PMC\Addons\Units\UI\uniform_combat_ls_bs_tp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Combat_LS_BS_TP_TB";
		};
	};*//*
	class PMC_Uniform_Combat_RS_BS_TP_TB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Combat_RS_BS_TP_TB";
		picture = "\PMC\Addons\Units\UI\uniform_combat_rs_bs_tp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Combat_RS_BS_TP_TB";
		};
	};*/
	class PMC_Uniform_Combat_LS_GS_BP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Combat_LS_GS_BP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_combat_ls_gs_bp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Combat_LS_GS_BP_BB";
		};
	};
	class PMC_Uniform_Combat_RS_GS_BP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Combat_RS_GS_BP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_combat_rs_gs_bp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Combat_RS_GS_BP_BB";
		};
	};
	class PMC_Uniform_Combat_LS_GS_TP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Combat_LS_GS_TP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_combat_ls_gs_tp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Combat_LS_GS_TP_BB";
		};
	};
	class PMC_Uniform_Combat_RS_GS_TP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Combat_RS_GS_TP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_combat_rs_gs_tp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Combat_RS_GS_TP_BB";
		};
	};
	class PMC_Uniform_Combat_LS_BS_DGP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Combat_LS_BS_DGP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_combat_ls_bs_dgp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Combat_LS_BS_DGP_BB";
		};
	};
	class PMC_Uniform_Combat_RS_BS_DGP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Combat_RS_BS_DGP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_combat_rs_bs_dgp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Combat_RS_BS_DGP_BB";
		};
	};
	class PMC_Uniform_Combat_LS_TS_BP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Combat_LS_TS_BP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_combat_ls_ts_bp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Combat_LS_TS_BP_BB";
		};
	};
	class PMC_Uniform_Combat_RS_TS_BP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Combat_RS_TS_BP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_combat_rs_ts_bp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Combat_RS_TS_BP_BB";
		};
	};/*
	class PMC_Uniform_Combat_LS_TS_GP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Combat_LS_TS_GP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_combat_ls_ts_gp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Combat_LS_TS_GP_BB";
		};
	};*//*
	class PMC_Uniform_Combat_RS_TS_GP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Combat_RS_TS_GP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_combat_rs_ts_gp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Combat_RS_TS_GP_BB";
		};
	};*/
	class PMC_Uniform_Combat_LS_TS_GP_TB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Combat_LS_TS_GP_TB";
		picture = "\PMC\Addons\Units\UI\uniform_combat_ls_ts_gp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Combat_LS_TS_GP_TB";
		};
	};
	class PMC_Uniform_Combat_RS_TS_GP_TB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Combat_RS_TS_GP_TB";
		picture = "\PMC\Addons\Units\UI\uniform_combat_rs_ts_gp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Combat_RS_TS_GP_TB";
		};
	};
	class PMC_Uniform_Combat_LS_CDBS_GP_TB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Combat_LS_CDBS_GP_TB";
		picture = "\PMC\Addons\Units\UI\uniform_combat_ls_cdbs_gp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Combat_LS_CDBS_GP_TB";
		};
	};
	class PMC_Uniform_Combat_RS_CDBS_GP_TB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Combat_RS_CDBS_GP_TB";
		picture = "\PMC\Addons\Units\UI\uniform_combat_rs_cdbs_gp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Combat_RS_CDBS_GP_TB";
		};
	};
	class PMC_Uniform_Combat_LS_CLBS_GP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Combat_LS_CLBS_GP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_combat_ls_clbs_gp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Combat_LS_CLBS_GP_BB";
		};
	};
	class PMC_Uniform_Combat_RS_CLBS_GP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Combat_RS_CLBS_GP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_combat_rs_clbs_gp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Combat_RS_CLBS_GP_BB";
		};
	};
	class PMC_Uniform_Combat_LS_CLRS_TP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Combat_LS_CLRS_TP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_combat_ls_clrs_tp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Combat_LS_CLRS_TP_BB";
		};
	};
	class PMC_Uniform_Combat_RS_CLRS_TP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Combat_RS_CLRS_TP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_combat_rs_clrs_tp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Combat_RS_CLRS_TP_BB";
		};
	};
	class PMC_Uniform_Combat_LS_CPS_BP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Combat_LS_CPS_BP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_combat_ls_cps_bp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Combat_LS_CPS_BP_BB";
		};
	};
	class PMC_Uniform_Combat_RS_CPS_BP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Combat_RS_CPS_BP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_combat_rs_cps_bp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Combat_RS_CPS_BP_BB";
		};
	};
	class PMC_Uniform_Garment_LS_BS_BP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Garment_LS_BS_BP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_garment_ls_bs_bp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Garment_LS_BS_BP_BB";
		};
	};
	class PMC_Uniform_Garment_RS_BS_BP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Garment_RS_BS_BP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_garment_rs_bs_bp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Garment_RS_BS_BP_BB";
		};
	};
	class PMC_Uniform_Garment_LS_BS_GP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Garment_LS_BS_GP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_garment_ls_bs_gp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Garment_LS_BS_GP_BB";
		};
	};
	class PMC_Uniform_Garment_RS_BS_GP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Garment_RS_BS_GP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_garment_rs_bs_gp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Garment_RS_BS_GP_BB";
		};
	};
	class PMC_Uniform_Garment_LS_OS_EP_TB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Garment_LS_OS_EP_TB";
		picture = "\PMC\Addons\Units\UI\uniform_garment_ls_os_ep_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Garment_LS_OS_EP_TB";
		};
	};
	class PMC_Uniform_Garment_RS_OS_EP_TB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Garment_RS_OS_EP_TB";
		picture = "\PMC\Addons\Units\UI\uniform_garment_rs_os_ep_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Garment_RS_OS_EP_TB";
		};
	};
	class PMC_Uniform_Garment_LS_GS_GP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Garment_LS_GS_GP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_garment_ls_gs_gp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Garment_LS_GS_GP_BB";
		};
	};
	class PMC_Uniform_Garment_RS_GS_GP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Garment_RS_GS_GP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_garment_rs_gs_gp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Garment_RS_GS_GP_BB";
		};
	};
	class PMC_Uniform_Garment_LS_GS_BP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Garment_LS_GS_BP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_garment_ls_gs_bp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Garment_LS_GS_BP_BB";
		};
	};
	class PMC_Uniform_Garment_RS_GS_BP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Garment_RS_GS_BP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_garment_rs_gs_bp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Garment_RS_GS_BP_BB";
		};
	};
	class PMC_Uniform_Garment_LS_GS_EP_TB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Garment_LS_GS_EP_TB";
		picture = "\PMC\Addons\Units\UI\uniform_garment_ls_gs_ep_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Garment_LS_GS_EP_TB";
		};
	};
	class PMC_Uniform_Garment_RS_GS_EP_TB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Garment_RS_GS_EP_TB";
		picture = "\PMC\Addons\Units\UI\uniform_garment_rs_gs_ep_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Garment_RS_GS_EP_TB";
		};
	};
	class PMC_Uniform_Garment_LS_ES_EP_TB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Garment_LS_ES_EP_TB";
		picture = "\PMC\Addons\Units\UI\uniform_garment_ls_es_ep_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Garment_LS_ES_EP_TB";
		};
	};
	class PMC_Uniform_Garment_RS_ES_EP_TB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Garment_RS_ES_EP_TB";
		picture = "\PMC\Addons\Units\UI\uniform_garment_rs_es_ep_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Garment_RS_ES_EP_TB";
		};
	};/*
	class PMC_Uniform_Garment_LS_ES_BP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Garment_LS_ES_BP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_garment_ls_es_bp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Garment_LS_ES_BP_BB";
		};
	};*//*
	class PMC_Uniform_Garment_RS_ES_BP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Garment_RS_ES_BP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_garment_rs_es_bp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Garment_RS_ES_BP_BB";
		};
	};*/
	class PMC_Uniform_Garment_LS_ES_GP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Garment_LS_ES_GP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_garment_ls_es_gp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Garment_LS_ES_GP_BB";
		};
	};
	class PMC_Uniform_Garment_RS_ES_GP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Garment_RS_ES_GP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_garment_rs_es_gp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Garment_RS_ES_GP_BB";
		};
	};
	class PMC_Uniform_Garment_LS_TS_TP_TB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Garment_LS_TS_TP_TB";
		picture = "\PMC\Addons\Units\UI\uniform_garment_ls_ts_tp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Garment_LS_TS_TP_TB";
		};
	};
	class PMC_Uniform_Garment_RS_TS_TP_TB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Garment_RS_TS_TP_TB";
		picture = "\PMC\Addons\Units\UI\uniform_garment_rs_ts_tp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Garment_RS_TS_TP_TB";
		};
	};/*
	class PMC_Uniform_Garment_LS_GS_TP_TB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Garment_LS_GS_TP_TB";
		picture = "\PMC\Addons\Units\UI\uniform_garment_ls_gs_tp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Garment_LS_GS_TP_TB";
		};
	};*//*
	class PMC_Uniform_Garment_RS_GS_TP_TB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Garment_RS_GS_TP_TB";
		picture = "\PMC\Addons\Units\UI\uniform_garment_rs_gs_tp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Garment_RS_GS_TP_TB";
		};
	};*/
	class PMC_Uniform_Garment_LS_TS_GP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Garment_LS_TS_GP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_garment_ls_ts_gp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Garment_LS_TS_GP_BB";
		};
	};
	class PMC_Uniform_Garment_RS_TS_GP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Garment_RS_TS_GP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_garment_rs_ts_gp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Garment_RS_TS_GP_BB";
		};
	};/*
	class PMC_Uniform_Garment_LS_OS_TP_TB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Garment_LS_OS_TP_TB";
		picture = "\PMC\Addons\Units\UI\uniform_garment_ls_os_tp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Garment_LS_OS_TP_TB";
		};
	};*//*
	class PMC_Uniform_Garment_RS_OS_TP_TB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Garment_RS_OS_TP_TB";
		picture = "\PMC\Addons\Units\UI\uniform_garment_rs_os_tp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Garment_RS_OS_TP_TB";
		};
	};*/
	class PMC_Uniform_Garment_LS_TS_BP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Garment_LS_TS_BP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_garment_ls_ts_bp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Garment_LS_TS_BP_BB";
		};
	};
	class PMC_Uniform_Garment_RS_TS_BP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Garment_RS_TS_BP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_garment_rs_ts_bp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Garment_RS_TS_BP_BB";
		};
	};
	class PMC_Uniform_Polo_TP_LS_TP_TB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Polo_TP_LS_TP_TB";
		picture = "\PMC\Addons\Units\UI\uniform_polo_tp_ls_tp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Polo_TP_LS_TP_TB";
		};
	};
	class PMC_Uniform_Polo_TP_TS_GP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Polo_TP_TS_GP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_polo_tp_ts_gp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Polo_TP_TS_GP_BB";
		};
	};
	class PMC_Uniform_Polo_TP_BS_TP_TB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Polo_TP_BS_TP_TB";
		picture = "\PMC\Addons\Units\UI\uniform_polo_tp_bs_tp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Polo_TP_BS_TP_TB";
		};
	};
	class PMC_Uniform_Polo_TP_BS_LP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Polo_TP_BS_LP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_polo_tp_bs_lp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Polo_TP_BS_LP_BB";
		};
	};
	class PMC_Uniform_Polo_TP_LS_GP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Polo_TP_LS_GP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_polo_tp_ls_gp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Polo_TP_LS_GP_BB";
		};
	};/*
	class PMC_Uniform_Polo_TP_OS_TP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Polo_TP_OS_TP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_polo_tp_os_tp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Polo_TP_OS_TP_BB";
		};
	};*/
	class PMC_Uniform_Polo_TP_OS_LP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Polo_TP_OS_LP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_polo_tp_os_lp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Polo_TP_OS_LP_BB";
		};
	};/*
	class PMC_Uniform_Polo_TP_GS_TP_TB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Polo_TP_GS_TP_TB";
		picture = "\PMC\Addons\Units\UI\uniform_polo_tp_gs_tp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Polo_TP_GS_TP_TB";
		};
	};*//*
	class PMC_Uniform_Polo_TP_WS_TP_TB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Polo_TP_WS_TP_TB";
		picture = "\PMC\Addons\Units\UI\uniform_polo_tp_ws_tp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Polo_TP_WS_TP_TB";
		};
	};*/
	class PMC_Uniform_Polo_TP_WS_LP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Polo_TP_WS_LP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_polo_tp_ws_lp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Polo_TP_WS_LP_BB";
		};
	};
	class PMC_Uniform_Polo_TP_WS_GP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Polo_TP_WS_GP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_polo_tp_ws_gp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Polo_TP_WS_GP_BB";
		};
	};
	class PMC_Uniform_Polo_CP_LS_TP_OB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Polo_CP_LS_TP_OB";
		picture = "\PMC\Addons\Units\UI\uniform_polo_cp_ls_tp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Polo_CP_LS_TP_OB";
		};
	};
	class PMC_Uniform_Polo_CP_RS_LP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Polo_CP_RS_LP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_polo_cp_rs_lp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Polo_CP_RS_LP_BB";
		};
	};/*
	class PMC_Uniform_Polo_CP_BS_TP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Polo_CP_BS_TP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_polo_cp_bs_tp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Polo_CP_BS_TP_BB";
		};
	};*/
	class PMC_Uniform_TShirt_JP_GS_LP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_TShirt_JP_GS_LP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_tshirt_jp_gs_lp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_TShirt_JP_GS_LP_BB";
		};
	};
	class PMC_Uniform_TShirt_JP_GS_TP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_TShirt_JP_GS_TP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_tshirt_jp_gs_tp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_TShirt_JP_GS_TP_BB";
		};
	};
	class PMC_Uniform_TShirt_JP_BS_LP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_TShirt_JP_BS_LP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_tshirt_jp_bs_lp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_TShirt_JP_BS_LP_BB";
		};
	};
	class PMC_Uniform_TShirt_JP_BS_TP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_TShirt_JP_BS_TP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_tshirt_jp_bs_tp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_TShirt_JP_BS_TP_BB";
		};
	};
	class PMC_Uniform_TShirt_JP_LS_LP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_TShirt_JP_LS_LP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_tshirt_jp_ls_lp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_TShirt_JP_LS_LP_BB";
		};
	};
	class PMC_Uniform_TShirt_JP_WS_LP_BB: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_TShirt_JP_WS_LP_BB";
		picture = "\PMC\Addons\Units\UI\uniform_tshirt_jp_ws_lp_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_TShirt_JP_WS_LP_BB";
		};
	};
	class PMC_Uniform_Flightsuit_Black: PMC_Uniform_Base {
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = "$STR_PMC_Units_Uniform_Flightsuit_Black";
		picture = "\PMC\Addons\Units\UI\icon_coveralls_black_ca.paa";
		class ItemInfo: ItemInfo {
			uniformClass = "PMC_Unit_Flightsuit_Black";
		};
	};
	
    // VIP
    class PMC_Suit_VIP: PMC_Uniform_Base {
        scope = 2;
        author = "Spartan Tactical Group, Pomigit, Jonpas";
        displayName = "$STR_PMC_Units_Suit_VIP";
        picture = "\PMC\Addons\Units\UI\suit_vip.paa";

        class ItemInfo: ItemInfo {
            containerClass = "Supply10";
            mass = 20;
            uniformClass = "PMC_Unit_I_VIP";
        };
    };
	
	/**
	 * WEAPON config
	 */

	class srifle_EBR_F;
	class srifle_EBR_SOS_bipod_snds_F: srifle_EBR_F {
		author="Optio";
		_generalMacro="srifle_EBR_MOS_bipod_snds_F";
		class LinkedItems {
			class LinkedItemsOptic {
				slot = "CowsSlot";
				item = "optic_SOS";
			};
			class LinkedItemsMuzzle {
				slot = "MuzzleSlot";
				item = "muzzle_snds_B";
			};
			class LinkedItemsUnder {
				slot = "UnderBarrelSlot";
				item = "bipod_01_F_blk";
			};
		};
	};
};
