class CfgPatches {
    class PMC_Headgear {
        units[] = {
            "PMC_Item_Cap_Dark",
            "PMC_Item_Cap_Backwards_Dark",
            "PMC_Item_Cap_Earpiece_Dark",
            "PMC_Item_Cap_Headphones_Dark",
            "PMC_Item_Cap_Headset_Dark",
            "PMC_Item_Cap_Backwards_ION",
            "PMC_Item_Cap_Earpiece_ION",
            "PMC_Item_Cap_Headset_ION",
            "PMC_Headgear_MilCap_blk",
			"PMC_Item_Helmet_CrewHeli_Skull"
        };
        weapons[] = {
            "PMC_Cap_Dark",
            "PMC_Cap_Backwards_Dark",
            "PMC_Cap_Headphones_Dark",
            "PMC_Cap_Earpiece_Dark",
            "PMC_Cap_Headset_Dark",
            "PMC_Cap_Backwards_ION",
            "PMC_Cap_Earpiece_ION",
            "PMC_Cap_Headset_ION",
            "PMC_MilCap_blk",
			"PMC_Helmet_CrewHeli_Skull"
        };
        requiredVersion = 0.1;
        requiredAddons[] = {"PMC_Common"};
        author = "Optio, Spartan Tactical Group, Pomigit, Jonpas, Rory";
    };
};

#include "CfgVehicles.hpp"
#include "CfgWeapons.hpp"
