class CfgVehicles {
	// Ground items
	#define MACRO_ITEM_COMMON \
		dlc = QUOTE(PREFIX); \
		scope = 2; \
		scopeCurator = 2; \
		vehicleClass = "ItemsVests";

	#define MACRO_ADDITEM(ITEM,COUNT) \
    class _xx_##ITEM { \
        name = #ITEM; \
        count = COUNT; \
    };

	class Vest_Base_F;
	// Name override
	
	class CLASS(Item_Vest_PlateCarrierFull_Black): Vest_Base_F {
		MACRO_ITEM_COMMON
		author = "Spartan Tactical Group, Jonpas";
		displayName = "$STR_PMC_Vests_Vest_PlateCarrierFull_Black";
		class TransportItems {
			MACRO_ADDITEM(PMC_Vest_PlateCarrierFull_Black,1)
		};
	};
	class CLASS(Item_Vest_PlateCarrierFull_Green): Vest_Base_F {
		MACRO_ITEM_COMMON
		author = "Spartan Tactical Group, Jonpas";
		displayName = "$STR_PMC_Vests_Vest_PlateCarrierFull_Green";

		class TransportItems {
			MACRO_ADDITEM(PMC_Vest_PlateCarrierFull_Green,1)
		};
	};
	class CLASS(Item_Vest_PlateCarrierFull_Olive): Vest_Base_F {
		MACRO_ITEM_COMMON
		author = "Spartan Tactical Group, Jonpas";
		displayName = "$STR_PMC_Vests_Vest_PlateCarrierFull_Olive";

		class TransportItems {
			MACRO_ADDITEM(PMC_Vest_PlateCarrierFull_Olive,1)
		};
	};
	class CLASS(Item_Vest_PlateCarrierIA1_blk): Vest_Base_F {
		MACRO_ITEM_COMMON
		author = "Optio";
		displayName = "$STR_PMC_Vests_Vest_PlateCarrierIAGL_Blk";

		class TransportItems {
			MACRO_ADDITEM(Vest_PlateCarrierIA1_blk,1)
		};
	};
	class CLASS(Item_Vest_PlateCarrierIA1_grn): Vest_Base_F {
		MACRO_ITEM_COMMON
		author = "Optio";
		displayName = "$STR_PMC_Vests_Vest_PlateCarrierIAGL_Grn";

		class TransportItems {
			MACRO_ADDITEM(Vest_PlateCarrierIA1_grn,1)
		};
	};
	/*
	class CLASS(Item_Vest_PlateCarrier_Black): Vest_Base_F {
		MACRO_ITEM_COMMON
		author = "Spartan Tactical Group, Jonpas";
		displayName = "$STR_PMC_Vests_Vest_PlateCarrier_Black";

		class TransportItems {
			MACRO_ADDITEM(PMC_Vest_PlateCarrier_Black,1)
		};
	};
	class CLASS(Item_Vest_PlateCarrier_Green): Vest_Base_F {
		MACRO_ITEM_COMMON
		author = "Spartan Tactical Group, Jonpas";
		displayName = "$STR_PMC_Vests_Vest_PlateCarrier_Green";

		class TransportItems {
			MACRO_ADDITEM(PMC_Vest_PlateCarrier_Green,1)
		};
	};
	class CLASS(Item_Vest_PlateCarrier_Coyote): Vest_Base_F {
		MACRO_ITEM_COMMON
		author = "Spartan Tactical Group, Jonpas";
		displayName = "$STR_PMC_Vests_Vest_PlateCarrier_Coyote";

		class TransportItems {
			MACRO_ADDITEM(PMC_Vest_PlateCarrier_Coyote,1)
		};
	};
	class CLASS(Item_Vest_PlateCarrier_Khaki): Vest_Base_F {
		MACRO_ITEM_COMMON
		author = "Spartan Tactical Group, Jonpas";
		displayName = "$STR_PMC_Vests_Vest_PlateCarrier_Khaki";

		class TransportItems {
			MACRO_ADDITEM(PMC_Vest_PlateCarrier_Khaki,1)
		};
	};
	class CLASS(Item_Vest_PlateCarrier_MARPAT): Vest_Base_F {
		MACRO_ITEM_COMMON
		author = "Spartan Tactical Group, Jonpas";
		displayName = "$STR_PMC_Vests_Vest_PlateCarrier_MARPAT";

		class TransportItems {
			MACRO_ADDITEM(PMC_Vest_PlateCarrier_MARPAT,1)
		};
	};
	*/
	class CLASS(Item_Vest_RangeBelt_Black): Vest_Base_F {
		MACRO_ITEM_COMMON
		author = "Spartan Tactical Group, Jonpas";
		displayName = "$STR_PMC_Vests_Vest_RangeBelt_Black";

		class TransportItems {
			MACRO_ADDITEM(PMC_Vest_RangeBelt_Black,1)
		};
	};
	class CLASS(Item_Vest_Tactical_DarkBlack): Vest_Base_F {
		MACRO_ITEM_COMMON
		author = "Spartan Tactical Group, Jonpas";
		displayName = "$STR_PMC_Vests_Vest_Tactical_DarkBlack";

		class TransportItems {
			MACRO_ADDITEM(PMC_Vest_Tactical_DarkBlack,1)
		};
	};

	class Vest_V_Press_F;
	class CLASS(Item_Vest_VIP): Vest_V_Press_F {
		displayName="Vest (VIP)";
		author="Optio";
		class TransportItems {
			MACRO_ADDITEM(PMC_Vest_VIP,1)
		};
	};

	// Box
	/*
	class B_supplyCrate_F;
	class CLASS(Box_Wearables): B_supplyCrate_F {
		class TransportItems {
			MACRO_ADDITEM(PMC_Vest_PlateCarrierFull_Black),10
			MACRO_ADDITEM(PMC_Vest_PlateCarrierFull_Green),10
			MACRO_ADDITEM(PMC_Vest_PlateCarrier_Black),10
			MACRO_ADDITEM(PMC_Vest_PlateCarrier_Green),10
			MACRO_ADDITEM(PMC_Vest_PlateCarrier_Coyote),10
			MACRO_ADDITEM(PMC_Vest_PlateCarrier_Khaki),10
			MACRO_ADDITEM(PMC_Vest_PlateCarrier_MARPAT),10
			MACRO_ADDITEM(PMC_Vest_RangeBelt_Black),10
			MACRO_ADDITEM(PMC_Vest_Tactical_DarkBlack),10
		};
	};*/
};
