class CfgWeapons {
	class HelmetBase;
	class H_Cap_headphones: HelmetBase {
		class ItemInfo;
	};
	
    #define MACRO_ITEM_COMMON \
        dlc = "PMC"; \
        scope = 2; \
        scopeCurator = 2; \
        vehicleClass = "ItemsHeadgear";
	
	#define QUOTE(var1) #var1
	#define CLASS(var1) PMC_##var1
	#define QCLASS(var1) QUOTE(CLASS(var1))
	#define CSTRING(var1) QUOTE($STR_PMC_Headgear_##var1)
	
	class H_MilCap_gry;
	class CLASS(MilCap_blk): H_MilCap_gry {
		dlc = "PMC";
		scope = 2;
		author = "Bohemia Interactive, Optio";
		displayName = CSTRING(MilCap_Black);
		picture = "\PMC\Addons\Headgear\UI\icon_h_milcap_blk_ca.paa";
		hiddenSelectionsTextures[] = {"\a3\characters_f_epb\common\data\cappatrol_blk_co.paa"};
	};
	
	class CLASS(Cap_Headphones_Dark): H_Cap_headphones {
		dlc = "PMC";
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas, Rory";
		displayName = CSTRING(Cap_Headphones_Dark);
		picture = "\PMC\Addons\Headgear\UI\cap_headphones_blk_ca.paa";
		hiddenSelectionsTextures[] = {"\PMC\Addons\Headgear\data\cap_headphones_blk_co.paa"};
		class ItemInfo: ItemInfo {
			hiddenSelections[] = {"camo"};
		};
	};

	class H_Cap_red: HelmetBase {
		class ItemInfo;
	};
	
	class CLASS(Cap_Dark): H_Cap_red {
		dlc = "PMC";
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas, Rory";
		displayName = CSTRING(Cap_Dark);
		picture = "\PMC\Addons\Headgear\UI\cap_blk_ca.paa";
		hiddenSelectionsTextures[] = {"\PMC\Addons\Headgear\data\cap_blk_co.paa"};
	};


	#define MACRO_EARPIECE_COMMON \
		dlc = "PMC"; \
		scope = 2; \
		model = "PMC\Addons\Headgear\data\cap_earpiece.p3d"; \
		class ItemInfo: ItemInfo { \
			mass = 5; \
			uniformModel = "PMC\Addons\Headgear\data\cap_earpiece.p3d"; \
		};

	class CLASS(Cap_Earpiece_Dark): H_Cap_red {
		MACRO_EARPIECE_COMMON
		author = "Spartan Tactical Group, Pomigit, Jonpas, Rory";
		displayname = CSTRING(Cap_Earpiece_Dark);
		picture = "\PMC\Addons\Headgear\UI\cap_earpiece_blk_ca.paa";
		hiddenSelectionsTextures[] = {"PMC\Addons\Headgear\data\cap_blk_co.paa"};
	};
	class CLASS(Cap_Earpiece_ION): CLASS(Cap_Earpiece_Dark) {
		author = "Optio";
		displayname = CSTRING(Cap_Earpiece_ION);
		picture = "\A3\characters_f\Data\UI\icon_H_Cap_blk_ION_CA.paa";
		hiddenSelectionsTextures[] = {"\A3\Characters_F\Common\Data\capb_ion_co.paa"};
	};

	#define MACRO_BACKWARDS_COMMON \
		dlc = "PMC"; \
		scope = 2; \
		model = "PMC\Addons\Headgear\data\cap_backwards.p3d"; \
		class ItemInfo: ItemInfo { \
			uniformModel = "PMC\Addons\Headgear\data\cap_backwards.p3d"; \
		};

	class CLASS(Cap_Backwards_Dark): H_Cap_red {
		MACRO_BACKWARDS_COMMON
		author = "Spartan Tactical Group, Pomigit, Jonpas, Rory";
		displayName = CSTRING(Cap_Backwards_Dark);
		picture = "\PMC\Addons\Headgear\UI\cap_backwards_blk_ca.paa";
		hiddenSelectionsTextures[] = {"PMC\Addons\Headgear\data\cap_blk_co.paa"};
	};
	class CLASS(Cap_Backwards_ION): CLASS(Cap_Backwards_Dark) {
		author = "Optio";
		displayname = CSTRING(Cap_Backwards_ION);
		picture = "\A3\characters_f\Data\UI\icon_H_Cap_blk_ION_CA.paa";
		hiddenSelectionsTextures[] = {"\A3\Characters_F\Common\Data\capb_ion_co.paa"};
	};

	class ItemCore;
	class H_HelmetB: ItemCore {
		class ItemInfo;
	};
	
	class H_CrewHelmetHeli_B: H_HelmetB {
		class ItemInfo;
	};
	
	class CLASS(Helmet_CrewHeli_Skull): H_CrewHelmetHeli_B {
		dlc = "PMC";
		scope = 2;
		author = "Spartan Tactical Group, Pomigit, Jonpas";
		displayName = CSTRING(Helmet_CrewHeli_Skull);
		picture = "\PMC\Addons\Headgear\UI\helmet_crewheli_black_ca.paa";
		hiddenSelectionsTextures[] = {"\PMC\Addons\Headgear\data\helmet_crewheli_skull_co.paa"};
		class ItemInfo: ItemInfo {
			modelSides[] = {6};
		};
	};
	
	class H_Cap_oli_hs;
	class CLASS(Cap_blk_hs): H_Cap_oli_hs {
		author = "Optio";
		displayName = CSTRING(Cap_Headset_Dark);
		picture = "\PMC\Addons\Headgear\UI\cap_blk_ca.paa";
		hiddenSelectionsTextures[] = {"\PMC\Addons\Headgear\data\cap_blk_co.paa"};
	};
	class CLASS(Cap_ION_hs): H_Cap_oli_hs {
		author = "Optio";
		displayName = CSTRING(Cap_Headset_ION);
		picture = "\A3\characters_f\Data\UI\icon_H_Cap_blk_ION_CA.paa";
		hiddenSelectionsTextures[] = {"\A3\Characters_F\Common\Data\capb_ion_co.paa"};
	};
	
};
