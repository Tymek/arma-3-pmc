#Arma 3 Private Military Mod
![Flag](https://bytebucket.org/Tymek/arma-3-pmc/raw/e9521a105ac2353b19c92de5f0c0461a4f596024/flag_ion.png)

Collection of handpicked PMC assets. NO CLAN BRANDING. Includes resources from unused textures (Missing Units by Sparfell, Littlebird Extension), older Arma games (CUP) and other modifications: TRYK's Multi-Play Uniforms, Spartan Tactical and Theseus Services.

__Source:__  [Repository on BitBucket](https://bitbucket.org/Tymek/arma-3-pmc/)

__License:__ [GPLv2](https://github.com/Theseus-Aegis/TheseusServices/blob/master/LICENSE) + [APC-SA](https://www.bistudio.com/community/licenses/arma-public-license-share-alike)

## Features
#### Vehicles
- AFV-4 Gorgon (Green) - with unarmed variant
- AMV-7 Marshall (Black) - with unarmed variant
- Offroad (Black) and Offroad Police - paint and config
- MH-9 Hummingbird and AH-9 Pawnee - Ion paint
- CH-49 Mohawk - Civilian and Ion paint

#### Uniforms and gear
- 40+ PMC contractor uniforms of various kind
- 10+ Civilian sets
- 8 Caps + 5 helmets
- 6 Vests + 1 belt piece
- 5 Backpacks

## Changelog
- Units - Gear (different vests)
- Units - Improvements (EOD, SL, TL)
- Headgear - Fixed PASGT helmet (hitpoints, picture)
- Headgear - heli crew helmet and military cap
- Headgear - Black and ION caps with gear
- Backpacks - Thesus Services / unlocked vanilla 
- Prowler LSV config for Editor and Zeus
- Gorgon (armed/unarmed)
- Published Marshall config (armed/unarmed)
- VIP unit, suit and vest
- Marksman unit
- Contractor unit
- Vests
- Progress for unarmed Marshall
- Two variants of black offroad
- Unbranding uniform assets
- Editor preview pictures
- Fixes for black Marshall - shininess, rivets (new depth map)
- CH-49 Mohawk retexturing
- MH-9/AH-9 retexturing (vanilla black texture)
- Uniforms - Spartan Tactical / Theseus Services (selected)
- AMV-7 Marshall Black (custom, with source from Spartan Tactical)

### TODO
- Selected TRYK's vests & backpacks
- Moar headgear
- Selected CUP Vehicles
- Infantry groups
- Randomized uniforms?
