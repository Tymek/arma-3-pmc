class CfgFactionClasses {
	class BLU_PMC_F {
		displayName = "Private Military";
		flag = "\A3\data_f\flags\flag_ion_co.paa";
		icon = "\PMC\Addons\Common\data\logo_ion_small_ca.paa";
		side = 1;
		priority = 3;
	};
	class IND_PMC_F: BLU_PMC_F {
		displayName = "Mercenaries";
		side = 2;
		priority = 2;
	};
};
